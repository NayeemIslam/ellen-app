import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'constants/app_color.dart';
import 'utils/helper/app_binding.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Ellen',
      theme: ThemeData(
        fontFamily: "Jost",
        scaffoldBackgroundColor: const Color.fromARGB(255, 240, 251, 255),
        appBarTheme: const AppBarTheme(
          titleSpacing: 0,
          color: Colors.white,
          elevation: 0.5,
          titleTextStyle: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: kDarkText,
            fontFamily: "Jost",
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
      getPages: AppRoute.getPage,
      defaultTransition: Transition.cupertino,
      initialRoute: AppRoute.splashPage,
      initialBinding: AppBinding(),
    );
  }
}
