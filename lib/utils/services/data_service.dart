import 'dart:convert';
import 'package:ellen_learning_app/module/model/course_model.dart';
import 'package:http/http.dart' as http;

import '../../constants/api_path.dart';

class ServiceApi {
  ServiceApi._();
  static final ServiceApi _instance = ServiceApi._();
  static ServiceApi get instance => _instance;

  Future courseNetworkCall() async {
    Uri uri = Uri.parse(course_api_path);
    try {
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        final _list = <CourseModel>[];
        final responseData = json.decode(response.body);

        responseData.forEach((e) {
          _list.add(CourseModel.fromJson(e));
        });

        return _list;
      }
    } catch (e) {
      rethrow;
    }
  }
}
