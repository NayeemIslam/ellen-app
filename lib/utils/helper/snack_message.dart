import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SnackMessage {
  void showSnackBar(String? message) {
    ScaffoldMessenger.of(Get.context!).hideCurrentSnackBar();
    ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(
        content: Text(message!.capitalize!),
      ),
    );
  }
}
