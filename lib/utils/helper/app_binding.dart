import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:ellen_learning_app/module/controller/cart_controller.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:get/get.dart';

class AppBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AppController>(() => AppController(), fenix: true);
    Get.lazyPut<CartController>(() => CartController(), fenix: true);
    Get.lazyPut<DataController>(() => DataController(), fenix: true);
  }
}
