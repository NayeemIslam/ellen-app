import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constants/app_color.dart';
import '../constants/responsive_size.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: 500.milliseconds,
      curve: Curves.ease,
      height: getScreenHeight(80),
      width: double.infinity,
      color: Colors.white,
      alignment: Alignment.center,
      padding: const EdgeInsets.only(left: 25, right: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Welcome,",
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w500,
                  color: kLightGreyColor,
                ),
              ),
              Text(
                "Jennifer Ronan",
                style: TextStyle(
                  fontSize: getTextSize(20),
                  fontWeight: FontWeight.w700,
                  color: kDarkText,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
