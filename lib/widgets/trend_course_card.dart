import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../constants/app_color.dart';
import '../constants/responsive_size.dart';

class TrendCard extends StatelessWidget {
  const TrendCard({
    Key? key,
    this.image,
    this.title,
    this.price,
    this.instructor,
  }) : super(key: key);
  final String? image;
  final String? title;
  final int? price;
  final String? instructor;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: const BorderRadius.vertical(top: Radius.circular(20)),
          child: Image.network(
            image!,
            height: getScreenHeight(160),
            fit: BoxFit.cover,
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          height: getScreenHeight(136),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                title!,
                maxLines: 2,
                minFontSize: 12,
                maxFontSize: 15,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                ),
              ),
              Text(
                instructor!,
                style: TextStyle(
                  fontSize: getTextSize(13),
                  fontWeight: FontWeight.w400,
                  color: kLightGreyColor,
                ),
              ),
              Text(
                "\$$price",
                style: TextStyle(
                  fontSize: getTextSize(17),
                  fontWeight: FontWeight.w400,
                  color: kNavyBlueColor,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
