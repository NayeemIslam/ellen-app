import 'package:flutter/material.dart';

import '../constants/responsive_size.dart';

class LabelHeadingText extends StatelessWidget {
  const LabelHeadingText({Key? key, @required this.text}) : super(key: key);
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text!.toUpperCase(),
      style: TextStyle(
        fontSize: getTextSize(16),
        fontWeight: FontWeight.w600,
      ),
    );
  }
}
