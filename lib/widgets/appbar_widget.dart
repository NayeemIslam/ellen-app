import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/module/controller/cart_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../constants/app_color.dart';
import '../constants/assets_path.dart';
import '../constants/responsive_size.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({Key? key, @required this.title, this.icon = true})
      : super(key: key);
  final String? title;
  final bool? icon;

  @override
  Widget build(BuildContext context) {
    // CartController cart = Get.find();
    return Container(
      height: getScreenHeight(60),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: Colors.white,
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Text(
            title!,
            style: TextStyle(
              fontSize: getTextSize(18),
              color: kDarkText,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}

class CartBadge extends StatelessWidget {
  const CartBadge({
    Key? key,
    this.top = 5,
  }) : super(key: key);
  final double? top;

  @override
  Widget build(BuildContext context) {
    CartController cart = Get.find();
    return InkWell(
      radius: 0,
      onTap: () => Get.toNamed(AppRoute.cartPage),
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          SvgPicture.asset(
            shoppingCart,
          ),
          Positioned(
            top: top,
            right: -5,
            child: CircleAvatar(
              backgroundColor: kBlueColor,
              radius: 10,
              child: Obx(() {
                return Center(
                  child: Text(
                    "${cart.cartItemsList.length}",
                    style: const TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
