import 'package:flutter/material.dart';

import '../constants/app_color.dart';
import '../constants/responsive_size.dart';

class TitleText extends StatelessWidget {
  const TitleText({
    Key? key,
    this.title,
    this.press,
  }) : super(key: key);
  final String? title;
  final VoidCallback? press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title!,
            style: TextStyle(
              fontSize: getTextSize(17),
              fontWeight: FontWeight.w600,
              color: kDarkText,
            ),
          ),
          InkWell(
            onTap: press,
            child: Text(
              "View All",
              style: TextStyle(
                fontSize: getTextSize(12),
                fontWeight: FontWeight.w400,
                color: kLightGreyColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
