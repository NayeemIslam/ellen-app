import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../constants/app_color.dart';
import '../constants/assets_path.dart';
import '../constants/responsive_size.dart';
import '../module/controller/app_controller.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    // AuthController auth = Get.find();
    // CartController cart = Get.find();
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            padding: EdgeInsets.zero,
            margin: const EdgeInsets.only(bottom: 20),
            child: Container(
              padding: const EdgeInsets.only(left: 20),
              color: kDrawerColor,
              child: Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Ellen",
                        style: TextStyle(
                          color: kNavyBlueColor,
                          fontSize: getTextSize(40),
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      getVerticalSpace(5),
                      Text(
                        "Best Learning\nOnline Platform".capitalize!,
                        style: TextStyle(
                          fontSize: getTextSize(16),
                          fontWeight: FontWeight.w400,
                          color: kNavyBlueColor,
                          letterSpacing: 0.2,
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                      right: 0, bottom: 0, child: Image.asset(drawerImg)),
                ],
              ),
            ),
          ),
          DrawerItem(
            title: "Wish-list",
            icon: favorite,
            press: () {
              if (appCtrl.bottomIndex.value == 3) {
                Get.back();
              } else {
                Get.back();
                appCtrl.changeHomePage(3);
              }
            },
          ),
          const CustomDivider(),
          DrawerItem(
              title: "My Courses",
              icon: myCourseSet,
              press: () {
                Get.offAndToNamed(AppRoute.authorCourse);
              }),
          const CustomDivider(),
          DrawerItem(
            title: "About Ellen",
            icon: about,
            press: () {
              Get.offAndToNamed(AppRoute.aboutPage);
            },
          ),
          const CustomDivider(),
          DrawerItem(
            title: "Blog",
            icon: blog,
            press: () {
              Get.offAndToNamed(AppRoute.blogPage);
            },
          ),
          const CustomDivider(),
          DrawerItem(
            title: "Terms & Conditions",
            icon: tc,
            press: () {
              Get.offAndToNamed(AppRoute.tcPage);
            },
          ),

          // if (auth.isUserExists.value)
          //   Column(
          //     mainAxisSize: MainAxisSize.min,
          //     children: [
          //       const CustomDivider(),
          //       DrawerItem(
          //         title: "Sign Out",
          //         icon: logoutIcon,
          //         textColor: kRed,
          //         press: () {
          //           final box = GetStorage();
          //           auth.isUserExists(false);
          //           box.write('token', null);
          //           box.write('id', null);
          //           auth.authModel(null);
          //           Get.back();
          //         },
          //       ),
          //     ],
          //   ),
        ],
      ),
    );
  }
}

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => const Divider(
        indent: 20,
        height: 10,
      );
}

class DrawerItem extends StatelessWidget {
  const DrawerItem({
    Key? key,
    this.title,
    this.press,
    this.icon,
    this.textColor = kDarkText,
  }) : super(key: key);

  final String? title;
  final VoidCallback? press;
  final String? icon;
  final Color? textColor;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      minLeadingWidth: 0,
      minVerticalPadding: 0,
      horizontalTitleGap: 15,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      leading: SvgPicture.asset(
        icon!,
        color: kDarkText,
        // height: getScreeWidth(20),
        // width: getScreeWidth(20),
      ),
      title: Text(title!,
          style: Get.textTheme.subtitle2!.copyWith(
            color: textColor,
            height: 0.5,
          )),
      trailing: const Icon(Icons.arrow_forward_ios, size: 10),
      onTap: press,
    );
  }
}
