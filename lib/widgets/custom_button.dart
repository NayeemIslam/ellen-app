import 'package:flutter/material.dart';

import '../constants/app_color.dart';
import '../constants/responsive_size.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {Key? key,
      this.title,
      this.press,
      this.color = kRedColor,
      this.isIcon = false})
      : super(key: key);
  final String? title;
  final VoidCallback? press;
  final bool? isIcon;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    // final CartController cart = Get.find();
    return SizedBox(
      // margin: const EdgeInsets.only(top: 30),
      width: double.infinity,
      height: getScreenHeight(55),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            )),
        onPressed: press,
        child: Text(
          title!,
          style: TextStyle(
            fontSize: getTextSize(16),
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
