import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../constants/app_color.dart';
import '../constants/app_constants.dart';
import '../constants/responsive_size.dart';
import '../module/controller/app_controller.dart';

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Container(
      height: getScreenHeight(80),
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: bottomNavItems
            .asMap()
            .entries
            .map(
              (e) => InkWell(
                onTap: () {
                  appCtrl.changeHomePage(e.key);
                },
                child: AnimatedContainer(
                  duration: 300.milliseconds,
                  curve: Curves.bounceOut,
                  padding: appCtrl.bottomIndex.value == e.key
                      ? const EdgeInsets.symmetric(horizontal: 15, vertical: 10)
                      : EdgeInsets.zero,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: appCtrl.bottomIndex.value == e.key
                        ? kBlueColor
                        : Colors.transparent,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SvgPicture.asset(
                        bottomNavItems[e.key]['icon'],
                        color: appCtrl.bottomIndex.value == e.key
                            ? Colors.white
                            : kGreyIconColor,
                      ),
                      appCtrl.bottomIndex.value == e.key
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                getHorizontalSpace(8),
                                Text(
                                  bottomNavItems[e.key]['title'],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: getTextSize(14),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
