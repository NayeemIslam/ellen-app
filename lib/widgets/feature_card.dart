import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../constants/app_color.dart';
import '../constants/responsive_size.dart';

class FeatureCard extends StatelessWidget {
  const FeatureCard({
    Key? key,
    this.image,
    this.title,
    this.price,
    this.instructor,
  }) : super(key: key);
  final String? image;
  final String? title;
  final int? price;
  final String? instructor;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            image!,
            height: getScreenHeight(120),
            fit: BoxFit.cover,
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
          height: getScreenHeight(115),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                title!,
                maxLines: 2,
                minFontSize: 12,
                maxFontSize: 15,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                ),
              ),
              Text(
                instructor!,
                style: TextStyle(
                  fontSize: getTextSize(13),
                  fontWeight: FontWeight.w400,
                  color: kLightGreyColor,
                ),
              ),
              RatingBar.builder(
                itemSize: 15,
                initialRating: 3,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: const EdgeInsets.only(right: 4.0),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                  size: 10,
                ),
                onRatingUpdate: (rating) {
                  debugPrint(rating.toString());
                },
              ),
              Text(
                "\$$price",
                style: TextStyle(
                  fontSize: getTextSize(17),
                  fontWeight: FontWeight.w500,
                  color: kBlueColor,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
