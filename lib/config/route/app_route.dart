import 'package:ellen_learning_app/module/view/blog/blog_list_page.dart';
import 'package:ellen_learning_app/module/view/others/about.dart';
import 'package:ellen_learning_app/module/view/others/policy.dart';
import 'package:ellen_learning_app/scroll_demo.dart';
import 'package:ellen_learning_app/core/auth/login_page.dart';
import 'package:ellen_learning_app/core/auth/register_page.dart';
import 'package:ellen_learning_app/module/view/blog/blog_details.dart';
import 'package:ellen_learning_app/module/view/cart/cart_page.dart';
import 'package:ellen_learning_app/module/view/cart/checkout_page.dart';
import 'package:ellen_learning_app/module/view/category/sole_course.dart';
import 'package:ellen_learning_app/module/view/details/course_details.dart';
import 'package:ellen_learning_app/module/view/instructor/creation_page.dart';
import 'package:ellen_learning_app/module/view/instructor/details_submisson.dart';
import 'package:ellen_learning_app/module/view/instructor/instructor_page.dart';
import 'package:ellen_learning_app/module/view/instructor/instructor_profile.dart';
import 'package:ellen_learning_app/module/view/landing_page.dart';
import 'package:ellen_learning_app/module/view/player/player.dart';
import 'package:ellen_learning_app/module/view/search/filter_page.dart';
import 'package:ellen_learning_app/module/view/search/search_page.dart';
import 'package:get/route_manager.dart';

import '../../module/view/courses/author_course.dart';
import '../../module/view/splash.dart';

class AppRoute {
  AppRoute._();
  static final AppRoute _instance = AppRoute._();
  static AppRoute get instance => _instance;

  static const String scrollDemo = "/demo";
  static const String splashPage = "/splash";
  static const String landPage = "/land";
  static const String searchPage = "/search";
  static const String cartPage = "/cart";
  static const String detailsPage = "/details";
  static const String playerPage = "/player";
  static const String singleCoursePage = "/singleCourse";
  static const String authorCourse = "/author_course";
  static const String loginPage = "/login";
  static const String registerPage = "/register";
  static const String checkoutPage = "/checkout";
  static const String instructorPage = "/instructor";
  static const String creationPage = "/creation";
  static const String submitPage = "/submit";
  static const String filterPage = "/filter";
  static const String instructorProfile = "/instructor_profile";
  static const String blogPage = "/blog";
  static const String blogDetails = "/blog_details";
  static const String aboutPage = "/about";
  static const String tcPage = "/tc";

  static List<GetPage> getPage = [
    GetPage(name: scrollDemo, page: () => const ScrollDemo()),
    GetPage(name: splashPage, page: () => const SplashPage()),
    GetPage(name: landPage, page: () => const LandingPage()),
    GetPage(name: searchPage, page: () => const SearchPage()),
    GetPage(name: cartPage, page: () => const CartPage()),
    GetPage(name: detailsPage, page: () => const CourseDetails()),
    GetPage(name: playerPage, page: () => const Player()),
    GetPage(name: singleCoursePage, page: () => const SingleCourse()),
    GetPage(name: authorCourse, page: () => const AuthorCourses()),
    GetPage(name: loginPage, page: () => const LoginPage()),
    GetPage(name: registerPage, page: () => const RegisterPage()),
    GetPage(name: checkoutPage, page: () => const CheckoutPage()),
    GetPage(name: instructorPage, page: () => const InstructorPage()),
    GetPage(name: creationPage, page: () => const CreationPage()),
    GetPage(name: submitPage, page: () => const SubmissionPage()),
    GetPage(name: filterPage, page: () => const FilterPage()),
    GetPage(name: instructorProfile, page: () => const InstructorProfile()),
    GetPage(name: blogPage, page: () => const BlogListPage()),
    GetPage(name: blogDetails, page: () => const BlogDetail()),
    GetPage(name: aboutPage, page: () => const AboutPage()),
    GetPage(name: tcPage, page: () => const TermsAndConPage()),
  ];
}
