import 'package:flutter/cupertino.dart';

const kDarkText = Color(0xff201E1C);
const kBgColor = Color(0xffF0FBFF);
const kBlueColor = Color(0xff08A9E6);
const kGreyIconColor = Color(0xff5E6074);
const kNavyBlueColor = Color(0xff21225F);
const kRedColor = Color(0xffEC272E);
const kLightGreyColor = Color(0xff808292);
const kLightTextColor = Color(0xff9499A7);
const kDeepPurpleColor = Color(0xff5352ED);
const kDeepOrangeColor = Color(0xffFF7F50);
const kDeepBrownColor = Color(0xffF9AF2A);
const kDeepBlueColor = Color(0xff1E90FF);
const kDrawerColor = Color(0xffBADEE9);
const kHintTextColor = Color(0xff8A8E99);
