import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/app_text.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/model/cart_model.dart';
import 'package:flutter/material.dart';

InputDecoration textInputDecoration({@required hint}) {
  return InputDecoration(
      hintText: hint,
      hintStyle: TextStyle(
        fontSize: getScreenHeight(14),
        fontWeight: FontWeight.w400,
        color: kHintTextColor,
      ),
      filled: true,
      fillColor: Colors.white,
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(color: Colors.transparent)),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(color: Colors.transparent)),
      disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(color: Colors.transparent)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(color: Colors.transparent)),
      alignLabelWithHint: true,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20));
}

List<Map<String, dynamic>> bottomNavItems = [
  {
    "icon": home,
    "title": "Home",
  },
  {
    "icon": category,
    "title": "Category",
  },
  {
    "icon": myCourse,
    "title": "My Courses",
  },
  {
    "icon": favorite,
    "title": "Wishlist",
  },
  {
    "icon": profile,
    "title": "Profile",
  },
];

List<String> tabItemsList = [
  "Web Development",
  "Digital Marketing",
  "Web Design",
  "Programming",
];

List<Map<String, dynamic>> topCategories = [
  {
    "color": kDeepBlueColor,
    "title": "Marketing",
    "total": 121,
    "avatar": "assets/images/11.png",
  },
  {
    "color": kDeepPurpleColor,
    "title": "Design",
    "total": 43,
    "avatar": "assets/images/13.png",
  },
  {
    "color": kDeepOrangeColor,
    "title": "Development",
    "total": 53,
    "avatar": "assets/images/14.png",
  },
  {
    "color": kDeepBrownColor,
    "title": "Business",
    "total": 24,
    "avatar": "assets/images/12.png",
  }
];
List<CartModel> trendingList = [
  CartModel(
    id: "1",
    title: "Agile Crash Course: Project Management",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/06.jpg",
  ),
  CartModel(
    id: "2",
    title: "Vue JS 3 - The Complete Guide Vue",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/07.jpg",
  ),
  CartModel(
    id: "3",
    title: "The Complete 2021 Web Developement",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/08.jpg",
  ),
];
List<CartModel> featuredList = [
  CartModel(
    id: "4",
    title: "User Experience Design Fundamental Course",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/09.jpg",
  ),
  CartModel(
    id: "5",
    title: "Fundamental Of Analyzing Real Estate",
    price: 83,
    instructor: "Json Andy",
    imgUrl: "assets/images/19.jpg",
  ),
  CartModel(
    id: "6",
    title: "Team Leadership & Team Management In 2020",
    price: 32,
    instructor: "Json Andy",
    imgUrl: "assets/images/20.jpg",
  ),
];

List<CartModel> studentViewList = [
  CartModel(
    id: "7",
    title: "How To Increase Your Communication Skills In 100 Days",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/22.jpg",
  ),
  CartModel(
    id: "8",
    title: "Advanced Objective oriented analysis of hard problems",
    price: 83,
    instructor: "Json Andy",
    imgUrl: "assets/images/23.jpg",
  ),
  CartModel(
    id: "9",
    title: "Learn Network Hacking From the Scratch in 2022",
    price: 32,
    instructor: "Json Andy",
    imgUrl: "assets/images/24.jpg",
  ),
];
List<CartModel> topItCourseList = [
  CartModel(
    id: "10",
    title: "User Experience Design Fundamental Course",
    price: 49,
    instructor: "Json Andy",
    imgUrl: "assets/images/27.jpg",
  ),
  CartModel(
    id: "11",
    title: "Fundamentals of analyzing Real Estate",
    price: 83,
    instructor: "Json Andy",
    imgUrl: "assets/images/28.jpg",
  ),
  CartModel(
    id: "12",
    title: "Team Leadership & Team Management in 2022",
    price: 32,
    instructor: "Json Andy",
    imgUrl: "assets/images/29.jpg",
  ),
];

List<Map<String, dynamic>> topcategoriesList = [
  {
    "title": "Web Development",
    "total": 15,
  },
  {
    "title": "Web Design",
    "total": 23,
  },
  {
    "title": "Crypto Currency",
    "total": 17,
  },
  {
    "title": "Mobile App Development",
    "total": 32,
  },
  {
    "title": "Javascript",
    "total": 25,
  },
  {
    "title": "Digital Marketing",
    "total": 42,
  },
  {
    "title": "Wordpress Development",
    "total": 42,
  },
];

List<String> courseOption = [
  "Edit Course",
  "Reviews",
  "Share This Course",
  "Delete This Course",
];
List<String> filterItems = ["Ratings", "Level", "Price"];
List<String> courseItems = [
  "Android Development",
  "Ios Development",
  "Web Development"
];
List<String> sortList = [
  "All Levels(1350)",
  "Beginners(122)",
  "Intermediate(824)",
  "Expert(404"
];
List<String> priceList = ["Paid(1350)", "Free(122)"];
List<String> ratingsList = [
  "4.5 & up (1350)",
  "4.0 & up (1225)",
  "3.5 & up (122)",
  "3.0 & up (124)"
];
List<Map<String, String>> paymentMethodList = [
  {
    "method_icon": paypal,
    "method_name": "Paypal",
  },
  {
    "method_icon": mastercard,
    "method_name": "Master Card",
  },
];
List<Map<String, String>> reasonToStart = [
  {
    "avatar": "assets/images/46.png",
    "title": "Teach In Your Own Way",
    "desc": loremText,
  },
  {
    "avatar": "assets/images/47.png",
    "title": "Inspire Your Learners",
    "desc": loremText,
  },
  {
    "avatar": "assets/images/48.png",
    "title": "Paid Off For Your Work",
    "desc": loremText,
  },
];
