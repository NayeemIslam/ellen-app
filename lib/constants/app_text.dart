const appName = "Ellen";
const splashText =
    "Start learning from the world’s best\ninstitutions through this app.";
const courseTitle = "User Experience Basic Design Fundamentals In 2022";
const courseDetail =
    "Design websites and mobile apps that yours love and return to again and again with uX experts json.";
const instructorTitle = "30 years UX Design Experience,\nConsultant, Author";
const reviewText =
    "Really taste very good. The service is also very good. Definately recommend this item. ";
const successText = "Congratulations! Your Enroll Is Successful";
const loremText =
    "Lorem ipsum dolor sit amet, consec teturad ipicing elit. Mauris ultrices cras sed commodo praesent dignissim proin ut.";
const blogText =
    "It is a long established fact that a reader\nwill be distracted by the readable content\nof a page when looking at its layout.";

const defaultInstructorProfileLink =
    "https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg";
List<String> privacyHeader = [
  "Out Website",
  "Data You Provide to Us",
  "How We Get Data About You",
  "What We Use Your Data For",
  "Your Choices About the Use of Your Data",
  "Our Policy Concerning Children",
  "Out Website",
  "Data You Provide to Us",
  "How We Get Data About You",
  "What We Use Your Data For",
  "Your Choices About the Use of Your Data",
  "Our Policy Concerning Children",
  "Out Website",
  "Data You Provide to Us",
  "How We Get Data About You",
  "What We Use Your Data For",
  "Your Choices About the Use of Your Data",
  "Our Policy Concerning Children",
];

const String loremStaticText =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.";
const String socialMediaText = "FOLLOW US ON:";

const String privacyDummyText =
    "Grow a Business Online From Scratch Make Money as an Affiliate Marketer Land a High-Paying Job in Digital Marketing.";
List<String> majorElement = [
  "Scientific skills for getting a better result.",
  "Communication skills to getting in touch.",
  "A career overview opportunity available.",
  "A good work environment for work.",
];

List<Map<String, String>> blogList = [
  {
    "tag": "Preschool",
    "img": "assets/images/09.jpg",
    "title": "Why Play Is Important in Preschool and Early",
    "date": "August 01, 2020",
  },
  {
    "tag": "Summer",
    "img": "assets/images/20.jpg",
    "title": "Surprising SUP Yoga Poses You’ll Want to Try This Summer",
    "date": "August 05, 2020",
  },
  {
    "tag": "YIT",
    "img": "assets/images/21.jpg",
    "title": "7 Things I Learned From Doing One of Those Social Media Yoga",
    "date": "August 12, 2020",
  },
  {
    "tag": "Heath Coaching",
    "img": "assets/images/22.jpg",
    "title": "10 Ways to Get Real About Your Body’s Limitations & Avoid",
    "date": "August 15, 2020",
  },
  {
    "tag": "Learning",
    "img": "assets/images/23.jpg",
    "title": "Surprising SUP Yoga Poses You’ll Want to Try This Summer",
    "date": "August 20, 2020",
  },
  {
    "tag": "Books",
    "img": "assets/images/24.jpg",
    "title": "Helping a local business reinvent itself",
    "date": "August 22, 2020",
  },
];
