//Splash assets
const splashUpper = "assets/images/splash_upper.png";
const splashLower = "assets/images/splash_lower.png";
const splash3 = "assets/images/splash_03.png";
const splash4 = "assets/images/splash_04.png";
const splash5 = "assets/images/splash_05.png";

//App Icons
const menu = "assets/icons/menu_icon.svg";
const shoppingCart = "assets/icons/shopping_cart.svg";
const filter = "assets/icons/filter-icon.svg";
const search = "assets/icons/search-icon.svg";
const back = "assets/icons/arrow-back.svg";
const tickCircle = "assets/icons/tick-circle.svg";
const uploadIcon = "assets/icons/upload.svg";

//NavBar Icons
const home = "assets/icons/home.svg";
const category = "assets/icons/category.svg";
const myCourse = "assets/icons/my_course.svg";
const favorite = "assets/icons/favorite_course.svg";
const profile = "assets/icons/profile.svg";

//Drawer Assets
const drawerImg = "assets/images/drawer-illustration.png";
const myCourseSet = "assets/icons/my_course_set.svg";
const about = "assets/icons/about.svg";
const blog = "assets/icons/blog.svg";
const tc = "assets/icons/t_c.svg";
const logout = "assets/icons/logout.svg";

const loginImage = "assets/images/login-image.png";
const registerImage = "assets/images/register-image.png";
const book = "assets/images/dialog-book.png";

//Payment method icon
const paypal = "assets/icons/paypal.svg";
const mastercard = "assets/icons/master-card.svg";

//
const tcImage = "assets/images/40.jpg";
const about1 = "assets/images/45.jpg";
const about2 = "assets/images/28.jpg";
const about3 = "assets/images/29.jpg";
