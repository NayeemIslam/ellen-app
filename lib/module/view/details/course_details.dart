import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/view/details/components/instructor.dart';
import 'package:ellen_learning_app/widgets/appbar_widget.dart';
import 'package:ellen_learning_app/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/assets_path.dart';
import 'components/curriculum.dart';
import 'components/specification.dart';

class CourseDetails extends StatelessWidget {
  const CourseDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        actions: [
          const CartBadge(),
          getHorizontalSpace(20),
        ],
      ),
      body: SingleChildScrollView(
        // padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Specification(),
            Curriculum(),
            Instructor(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        height: getScreenHeight(80),
        color: Colors.white,
        child: CustomButton(title: "Enroll Now", press: () {}),
      ),
    );
  }
}
