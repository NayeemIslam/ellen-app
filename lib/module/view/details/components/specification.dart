import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/module/controller/cart_controller.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../../constants/app_color.dart';
import '../../../../constants/assets_path.dart';
import '../../../../constants/responsive_size.dart';

class Specification extends StatelessWidget {
  const Specification({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CartController cart = Get.find();
    DataController data = Get.find();
    return Column(
      children: [
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
              // width: getScreeWidth(340),
              height: getScreenHeight(190),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: NetworkImage(
                    data.courseDetails.value.image!,
                  ),
                  fit: BoxFit.fitWidth,
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.5),
                    BlendMode.darken,
                  ),
                ),
              ),
              margin: const EdgeInsets.all(10.0),
            ),
            InkWell(
              onTap: () {
                Get.toNamed(AppRoute.playerPage);
              },
              child: CircleAvatar(
                radius: 40,
                backgroundColor: Colors.white.withOpacity(0.35),
                child: const Center(
                  child: Icon(
                    Icons.play_arrow_rounded,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 25,
              child: Text(
                "Preview This  Course",
                style: TextStyle(
                  fontSize: getTextSize(13),
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.courseDetails.value.name!,
                style: TextStyle(
                  fontSize: getTextSize(20),
                  fontWeight: FontWeight.w500,
                  color: kNavyBlueColor,
                ),
              ),
              getVerticalSpace(8),
              Text(
                data.courseDetails.value.excerpt!,
                style: TextStyle(
                  fontSize: getTextSize(13),
                  fontWeight: FontWeight.w400,
                  color: kLightGreyColor,
                ),
              ),
              getVerticalSpace(10),
              RichText(
                text: TextSpan(
                  text: "Created By\t",
                  style: TextStyle(
                    fontSize: getTextSize(13),
                    fontWeight: FontWeight.w400,
                    color: kDarkText,
                  ),
                  children: [
                    TextSpan(
                      text: data.courseDetails.value.instructor!.name,
                      style: TextStyle(
                        fontSize: getTextSize(13),
                        fontWeight: FontWeight.w600,
                        color: kDarkText,
                      ),
                    ),
                  ],
                ),
              ),
              getVerticalSpace(15),
              RatingBar.builder(
                itemSize: 15,
                initialRating: 3,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: const EdgeInsets.only(right: 4.0),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                  size: 10,
                ),
                onRatingUpdate: (rating) {
                  debugPrint(rating.toString());
                },
              ),
              getVerticalSpace(15),
              RichText(
                text: TextSpan(
                  text: "USD\t",
                  style: TextStyle(
                    fontSize: getTextSize(14),
                    fontWeight: FontWeight.w400,
                    color: kBlueColor,
                  ),
                  children: [
                    TextSpan(
                      text: "\$${data.courseDetails.value.price}",
                      style: TextStyle(
                        fontSize: getTextSize(20),
                        fontWeight: FontWeight.w600,
                        color: kBlueColor,
                      ),
                    ),
                  ],
                ),
              ),
              getVerticalSpace(20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ...List.generate(
                    2,
                    (index) => SizedBox(
                      height: getScreenHeight(55),
                      width: getScreeWidth(152),
                      // margin: EdgeInsets.only(right: index == 0 ? 15 : 0),
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6)),
                          side: BorderSide(
                            color: index == 0 ? kDeepPurpleColor : kBlueColor,
                          ),
                        ),
                        onPressed: () {
                          switch (index) {
                            case 0:
                              cart.addToCart(cart.courseDetails);
                              break;
                            case 1:
                              cart.addToFavorite(cart.courseDetails);
                              break;
                            default:
                              break;
                          }
                        },
                        child: Text(
                          index == 0 ? "Add To Cart" : "Wishlist",
                          style: TextStyle(
                            fontSize: getTextSize(16),
                            fontWeight: FontWeight.w500,
                            color: index == 0 ? kDeepPurpleColor : kBlueColor,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              getVerticalSpace(30),
              Text(
                "What You Will Learn",
                style: TextStyle(
                  fontSize: getTextSize(17),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                ),
              ),
              getVerticalSpace(10),
              ...List.generate(
                4,
                (index) => Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      SvgPicture.asset(tickCircle),
                      getHorizontalSpace(10),
                      Expanded(
                        child: Text(
                          "Understand user experience design and why it's important",
                          style: TextStyle(
                            fontSize: getTextSize(16),
                            fontWeight: FontWeight.w400,
                            color: kLightGreyColor,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
