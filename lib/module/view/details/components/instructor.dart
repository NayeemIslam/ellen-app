import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:ellen_learning_app/module/view/details/components/review.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/app_text.dart';
import '../../../../constants/responsive_size.dart';

class Instructor extends StatelessWidget {
  const Instructor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DataController data = Get.find();
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Instructor",
            style: TextStyle(
              fontSize: getTextSize(17),
              fontWeight: FontWeight.w600,
              color: kNavyBlueColor,
            ),
          ),
          getVerticalSpace(10),
          const InstructorCard(
            radius: 50,
            titleTextSize: 17,
            singleCard: true,
          ),
          getVerticalSpace(20),
          SizedBox(
            height: getScreenHeight(55),
            width: double.infinity,
            child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)),
                  side: const BorderSide(
                    color: kBlueColor,
                  ),
                ),
                onPressed: () {
                  data.fetchInstructorInfo(data.courseDetails.value.instructor!);
                  Get.toNamed(AppRoute.instructorProfile);
                },
                child: Text(
                  "View Profile",
                  style: TextStyle(
                    fontSize: getTextSize(16),
                    fontWeight: FontWeight.w500,
                    color: kBlueColor,
                  ),
                )),
          ),
          getVerticalSpace(30),
          const ReviewSegment(),
        ],
      ),
    );
  }
}

class InstructorCard extends StatelessWidget {
  const InstructorCard({
    Key? key,
    this.titleTextSize,
    this.radius,
    this.singleCard = false,
  }) : super(key: key);
  final double? titleTextSize;
  final double? radius;
  final bool? singleCard;

  @override
  Widget build(BuildContext context) {
    DataController data = Get.find();
    return Obx(() {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: radius!,
            backgroundImage: NetworkImage(
                data.courseDetails.value.instructor!.avatar ??
                    defaultInstructorProfileLink),
          ),
          getHorizontalSpace(15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.courseDetails.value.instructor!.name!,
                  style: TextStyle(
                    fontSize: getTextSize(titleTextSize!),
                    fontWeight: FontWeight.w600,
                    color: kNavyBlueColor,
                  ),
                ),
                getVerticalSpace(8),
                Text(
                  instructorTitle,
                  style: TextStyle(
                    fontSize: getTextSize(13),
                    fontWeight: FontWeight.w400,
                    color: kLightGreyColor,
                  ),
                ),
                getVerticalSpace(8),
                Row(
                  children: [
                    const StudentNumber(),
                    getHorizontalSpace(6),
                    if (singleCard!)
                      const CircleAvatar(
                        radius: 4,
                        backgroundColor: kBlueColor,
                      ),
                    getHorizontalSpace(6),
                    if (singleCard!) const CourseNumber(),
                  ],
                ),
                getVerticalSpace(5),
                if (!singleCard!) const CourseNumber(),
              ],
            ),
          )
        ],
      );
    });
  }
}

class CourseNumber extends StatelessWidget {
  const CourseNumber({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "11\t",
        style: TextStyle(
          fontSize: getTextSize(13),
          fontWeight: FontWeight.w400,
          color: kDarkText,
        ),
        children: [
          TextSpan(
            text: "Courses",
            style: TextStyle(
              fontSize: getTextSize(13),
              fontWeight: FontWeight.w400,
              color: kLightGreyColor,
            ),
          ),
        ],
      ),
    );
  }
}

class StudentNumber extends StatelessWidget {
  const StudentNumber({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "2350\t",
        style: TextStyle(
          fontSize: getTextSize(13),
          fontWeight: FontWeight.w400,
          color: kDarkText,
        ),
        children: [
          TextSpan(
            text: "Students",
            style: TextStyle(
              fontSize: getTextSize(13),
              fontWeight: FontWeight.w400,
              color: kLightGreyColor,
            ),
          ),
        ],
      ),
    );
  }
}
