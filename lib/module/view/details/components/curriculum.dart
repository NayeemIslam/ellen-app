import 'package:flutter/material.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/responsive_size.dart';

class Curriculum extends StatelessWidget {
  const Curriculum({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              "Curriculum",
              style: TextStyle(
                fontSize: getTextSize(17),
                fontWeight: FontWeight.w600,
                color: kDarkText,
              ),
            ),
          ),
          getVerticalSpace(10),
          ...List.generate(
            4,
            (index) => ExpansionTile(
              childrenPadding: const EdgeInsets.all(20),

              // maintainState: true,
              // backgroundColor: kBlueColor,
              // collapsedBackgroundColor: kBlueColor,
              title: Text(
                "Section ${index + 1}",
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w500,
                  color: kNavyBlueColor,
                ),
              ),
              tilePadding: const EdgeInsets.symmetric(horizontal: 20),
              trailing: const CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(Icons.add),
              ),
              onExpansionChanged: (v) {
                debugPrint("$v");
              },
              children: [
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text(
                        "01",
                        style: TextStyle(
                          fontSize: getTextSize(14),
                          fontWeight: FontWeight.w500,
                          color: kDarkText,
                        ),
                      ),
                      getHorizontalSpace(20),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "Introduction",
                              style: TextStyle(
                                fontSize: getTextSize(14),
                                fontWeight: FontWeight.w400,
                                color: kDarkText,
                              ),
                            ),
                            Text(
                              "4:24 mins",
                              style: TextStyle(
                                fontSize: getTextSize(12),
                                fontWeight: FontWeight.w400,
                                color: kGreyIconColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      getHorizontalSpace(20),
                      CircleAvatar(
                        backgroundColor: kBlueColor.withOpacity(0.07),
                        child: const Icon(
                          Icons.play_arrow_rounded,
                          color: kBlueColor,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
