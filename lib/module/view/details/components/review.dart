import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/app_text.dart';
import '../../../../constants/responsive_size.dart';

class ReviewSegment extends StatelessWidget {
  const ReviewSegment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Reviews",
          style: TextStyle(
            fontSize: getTextSize(17),
            fontWeight: FontWeight.w600,
            color: kNavyBlueColor,
          ),
        ),
        getVerticalSpace(10),
        ...List.generate(
          2,
          (index) => Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getVerticalSpace(15),
              Row(
                children: [
                  const CircleAvatar(
                    backgroundColor: Colors.transparent,
                    backgroundImage: AssetImage('assets/images/32.jpg'),
                  ),
                  getHorizontalSpace(10),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Suvam S.",
                        style: TextStyle(
                          fontSize: getTextSize(14),
                          fontWeight: FontWeight.w500,
                          color: kDarkText,
                        ),
                      ),
                      getVerticalSpace(6),
                      Text(
                        "12th Jan, 2022",
                        style: TextStyle(
                          fontSize: getTextSize(12),
                          fontWeight: FontWeight.w500,
                          color: kGreyIconColor,
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  RatingBar.builder(
                    itemSize: 15,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.only(right: 4.0),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                      size: 10,
                    ),
                    onRatingUpdate: (rating) {
                      debugPrint(rating.toString());
                    },
                  ),
                ],
              ),
              getVerticalSpace(15),
              Text(
                reviewText,
                style: TextStyle(
                  fontSize: getTextSize(14),
                  fontWeight: FontWeight.w500,
                  color: kGreyIconColor,
                ),
              ),
              getVerticalSpace(15),
            ],
          ),
        ),
      ],
    );
  }
}
