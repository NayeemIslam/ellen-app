import 'package:chewie/chewie.dart';
import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

import '../../../constants/responsive_size.dart';

class Player extends StatefulWidget {
  const Player({Key? key}) : super(key: key);

  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;
  // CourseController? courseController;
  // int courseIndex;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // courseController = Get.find();
    _load();
  }

  _load() {
    // courseController.enrolledVideos[courseIndex].videoUrl
    try {
      // courseIndex = ModalRoute.of(context).settings.arguments as int;
      _controller = VideoPlayerController.network(
          "https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4");
      _init();
      setState(() {});
    } on Exception catch (e) {
      debugPrint(e.toString());
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future _init() async {
    await _controller!.initialize();
    chewieController = ChewieController(
      videoPlayerController: _controller!,
      autoPlay: false,
      looping: true,
      showControls: true,
      allowFullScreen: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Colors.blue,
        handleColor: Colors.blue,
        bufferedColor: Colors.lightGreen,
        backgroundColor: Colors.black,
      ),
      // placeholder: Container(color: KprimaryColor),
      autoInitialize: true,
      // aspectRatio: 1,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Text(errorMessage, style: const TextStyle(color: Colors.white)),
          ),
        );
      },
    );

    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    if (mounted) _controller!.dispose();
    if (mounted) chewieController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Column(
      children: [
        _videoPlayer(),
        // _playerList(),
      ],
    );
  }

  Expanded _videoPlayer() {
    return Expanded(
      child: chewieController != null &&
              chewieController!.videoPlayerController.value.isInitialized
          ? Padding(
              padding: const EdgeInsets.all(3.0),
              child: Chewie(controller: chewieController!))
          : Container(),
    );
  }

  // Expanded _playerList() {
  //   return Expanded(
  //     flex: 2,
  //     child: Padding(
  //       padding: const EdgeInsets.all(20.0),
  //       child: ListView.builder(
  //         itemCount: courseController.enrolledVideos.length,
  //         itemBuilder: (ctx, index) {
  //           final _videoItem = courseController.enrolledVideos[index];
  //           return Column(
  //             mainAxisSize: MainAxisSize.min,
  //             children: [
  //               Row(
  //                 children: [
  //                   Expanded(
  //                     child: ClipRRect(
  //                       borderRadius: BorderRadius.circular(5),
  //                       child: Image.network(_videoItem.course.profilePhoto,
  //                           height: getScreenHeight(90), fit: BoxFit.cover),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     flex: 2,
  //                     child: Padding(
  //                       padding: const EdgeInsets.only(left: 10.0),
  //                       child: GestureDetector(
  //                         onTap: () {
  //                           courseIndex = index;
  //                           print(courseIndex);
  //                           setState(() {});
  //                         },
  //                         child: _listText(index, _videoItem),
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //               getVerticalSpace(10),
  //               if (index != courseController.enrolledVideos.length - 1)
  //                 Divider(color: Colors.black),
  //               getVerticalSpace(10),
  //             ],
  //           );
  //         },
  //       ),
  //     ),
  //   );
  // }

  // Column _listText(int index, Videos _videoItem) {
  //   return Column(
  //     crossAxisAlignment: CrossAxisAlignment.start,
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     mainAxisSize: MainAxisSize.min,
  //     children: [
  //       Text(
  //         "Lesson - ${index + 1}",
  //         style: TextStyle(
  //             fontSize: getTextSize(11),
  //             color: primaryColor,
  //             fontWeight: FontWeight.w700),
  //       ),
  //       getVerticalSpace(5),
  //       Text(
  //         _videoItem.course.title,
  //         overflow: TextOverflow.ellipsis,
  //         style: TextStyle(
  //             fontSize: getTextSize(14),
  //             color: darkText,
  //             fontWeight: FontWeight.w800),
  //       ),
  //       getVerticalSpace(10),
  //       Row(
  //         children: [
  //           SvgPicture.asset('assets/icons/notebook.svg'),
  //           getHorizontalSpace(5),
  //           Text(_videoItem.name.substring(0, 10) + "..."),
  //         ],
  //       ),
  //     ],
  //   );
  // }

  AppBar _buildAppBar() {
    return AppBar(
      title: _title(),
      titleSpacing: 0.0,
      leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: kDarkText),
          onPressed: () => Get.back()),
      backgroundColor: kBgColor,
      elevation: 0.0,
      // actions: [],
    );
  }

  Text _title() {
    return Text(
      "Course Videos",
      style: TextStyle(
          fontWeight: FontWeight.w800,
          fontSize: getTextSize(20),
          color: kDarkText),
    );
  }
}
