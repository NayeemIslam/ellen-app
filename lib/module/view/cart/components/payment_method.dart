import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../constants/app_constants.dart';
import '../../../../constants/responsive_size.dart';
import '../../../../widgets/label_heading.dart';
import '../../../controller/app_controller.dart';

class PaymentMethod extends StatelessWidget {
  const PaymentMethod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const LabelHeadingText(text: "Payment Method"),
        getVerticalSpace(15),
        ...List.generate(
          paymentMethodList.length,
          (index) => Obx(
            () => InkWell(
              onTap: () {
                appCtrl.changePaymetMethod(index);
              },
              child: AnimatedContainer(
                duration: 500.milliseconds,
                curve: Curves.ease,
                height: getScreenHeight(60),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: const Color(0xffE8E8E8),
                      width: 1,
                    )),
                padding: const EdgeInsets.symmetric(horizontal: 15),
                margin: const EdgeInsets.only(bottom: 10),
                alignment: Alignment.center,
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset(
                        paymentMethodList[index]['method_icon'].toString()),
                    getHorizontalSpace(10),
                    Text(
                      paymentMethodList[index]['method_name'].toString(),
                      style: const TextStyle(
                        color: kDarkText,
                        fontSize: 14,
                      ),
                    ),
                    const Spacer(),
                    CircleAvatar(
                      radius: 12,
                      backgroundColor: appCtrl.paymentMethodIndex.value == index
                          ? kBlueColor
                          : const Color(0xffDBDCDF),
                      child: const Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 15,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
