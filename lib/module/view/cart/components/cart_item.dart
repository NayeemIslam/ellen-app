import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/responsive_size.dart';
import '../../../controller/cart_controller.dart';
import '../../../model/cart_model.dart';

class CartItem extends StatelessWidget {
  const CartItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  final CartModel item;

  @override
  Widget build(BuildContext context) {
    CartController cart = Get.find();
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 0),
      // height: getScreenHeight(85),
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.asset(
                  item.imgUrl!,
                  height: getScreenHeight(80),
                  width: getScreeWidth(80),
                  fit: BoxFit.cover,
                ),
              ),
              getHorizontalSpace(10),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      item.title!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: getTextSize(16),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    getVerticalSpace(4),
                    Text(
                      item.instructor!,
                      style: TextStyle(
                        fontSize: getTextSize(13),
                        fontWeight: FontWeight.w400,
                        color: kGreyIconColor,
                      ),
                    ),
                    getVerticalSpace(4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "\$${item.price!.toStringAsFixed(2)}",
                          style: TextStyle(
                            fontSize: getTextSize(17),
                            fontWeight: FontWeight.w600,
                            color: kBlueColor,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            cart.removeFromCart(item.id);
                          },
                          child: Text(
                            "REMOVE",
                            style: TextStyle(
                              fontSize: getTextSize(11),
                              fontWeight: FontWeight.w500,
                              color: kRedColor,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
