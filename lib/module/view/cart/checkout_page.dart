import 'package:ellen_learning_app/module/view/cart/components/payment_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_color.dart';
import '../../../constants/app_constants.dart';
import '../../../constants/app_text.dart';
import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/label_heading.dart';

class CheckoutPage extends StatefulWidget {
  const CheckoutPage({Key? key}) : super(key: key);

  @override
  State<CheckoutPage> createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  TextEditingController nameCtrl = TextEditingController();
  TextEditingController emailCtrl = TextEditingController();
  TextEditingController phoneCtrl = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    nameCtrl.dispose();
    emailCtrl.dispose();
    phoneCtrl.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: const Text("Checkout"),
      ),
      body: ListView(
        // crossAxisAlignment: CrossAxisAlignment.start,
        padding: const EdgeInsets.all(20),
        children: [
          const LabelHeadingText(text: "information"),
          getVerticalSpace(20),
          TextField(
            controller: nameCtrl,
            decoration: textInputDecoration(hint: "Full Name"),
          ),
          getVerticalSpace(10),
          TextField(
            controller: emailCtrl,
            decoration: textInputDecoration(hint: "Email Address"),
          ),
          getVerticalSpace(10),
          TextField(
            controller: phoneCtrl,
            decoration: textInputDecoration(hint: "Phone No"),
          ),
          getVerticalSpace(30),
          const PaymentMethod(),
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        height: getScreenHeight(80),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: CustomButton(
          title: "Enroll Now",
          press: () {
            Get.dialog(Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              child: const DialogCard(),
            ));
          },
        ),
      ),
    );
  }
}

class DialogCard extends StatelessWidget {
  const DialogCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      // height: getScreenHeight(300),
      // alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(book),
          getVerticalSpace(40),
          Text(
            successText.capitalize!,
            textAlign: TextAlign.center,
            maxLines: 3,
            style: TextStyle(
                fontSize: getTextSize(18),
                fontWeight: FontWeight.w700,
                height: 1.5,
                letterSpacing: -0.3,
                color: kDarkText),
          ),
          getVerticalSpace(40),
          SizedBox(
            width: getScreeWidth(180),
            child: CustomButton(
                title: "Got To My Courses",
                press: () {
                  Get.back();
                }),
          ),
        ],
      ),
    );
  }
}
