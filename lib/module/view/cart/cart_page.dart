import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/module/controller/cart_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../config/route/app_route.dart';
import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import '../../../widgets/custom_button.dart';
import 'components/cart_item.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CartController cartCtrl = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: const Text("Cart"),
      ),
      body: Obx(() {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Text(
                "${cartCtrl.cartItemsList.length} Item(s) in the cart"
                    .capitalize
                    .toString(),
                style: TextStyle(
                  fontSize: getTextSize(16),
                  fontWeight: FontWeight.w600,
                  color: kGreyIconColor,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: ListView.builder(
                itemCount: cartCtrl.cartItemsList.length,
                itemBuilder: (ctx, index) {
                  final item = cartCtrl.cartItemsList[index];
                  return CartItem(item: item);
                },
              ),
            ),
            getVerticalSpace(10),
          ],
        );
      }),
      bottomNavigationBar: Obx(() {
        return Container(
          color: Colors.white,
          height: getScreenHeight(80),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: CustomButton(
            title: "Proceed To Checkout",
            press: cartCtrl.cartItemsList.isNotEmpty
                ? () {
                    Get.toNamed(AppRoute.checkoutPage);
                  }
                : null,
          ),
        );
      }),
    );
  }
}
