import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../home/components/top_category.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            physics: const BouncingScrollPhysics(),
            children: topCategories
                .asMap()
                .entries
                .map(
                  (e) => Padding(
                    padding: EdgeInsets.only(
                      bottom: topCategories.length - 1 == e.key ? 0 : 15.0,
                    ),
                    child: InkWell(
                      onTap: () {
                        Get.toNamed(AppRoute.singleCoursePage,
                            arguments: topCategories[e.key]['title']);
                      },
                      child: CategoryCard(
                        height: 150,
                        titleTextSize: 20,
                        index: e.key,
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
        )
      ],
    );
  }
}
