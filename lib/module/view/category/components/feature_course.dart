import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/widgets/feature_card.dart';
import 'package:flutter/material.dart';

import '../../../../constants/app_constants.dart';
import '../../../../constants/responsive_size.dart';

class FeatureCourse extends StatelessWidget {
  const FeatureCourse({Key? key, @required this.course}) : super(key: key);
  final String? course;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "Featured In $course",
            style: TextStyle(
              fontSize: getTextSize(17),
              fontWeight: FontWeight.w600,
              color: kDarkText,
            ),
          ),
        ),
        getVerticalSpace(15),
        SizedBox(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                ...List.generate(
                  studentViewList.length,
                  (index) => Container(
                    margin: EdgeInsets.only(
                        right: studentViewList.length - 1 == index ? 0 : 15),
                    height: getScreenHeight(280),
                    width: getScreeWidth(140),
                    child: FeatureCard(
                      image: studentViewList[index].imgUrl,
                      title: studentViewList[index].title,
                      instructor: studentViewList[index].instructor,
                      price: studentViewList[index].price,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
