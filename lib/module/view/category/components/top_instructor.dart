import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';

import '../../../../constants/responsive_size.dart';
import '../../details/components/instructor.dart';

class TopInstructor extends StatelessWidget {
  const TopInstructor({Key? key, @required this.course}) : super(key: key);
  final String? course;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Text(
            "Top Instructors In $course",
            style: TextStyle(
              fontSize: getTextSize(17),
              fontWeight: FontWeight.w600,
              color: kDarkText,
            ),
          ),
        ),
        // getVerticalSpace(15),
        SizedBox(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                ...List.generate(
                  3,
                  (index) => SizedBox(
                    height: getScreenHeight(120),
                    width: getScreeWidth(260),
                    child: const InstructorCard(
                      radius: 40,
                      titleTextSize: 14,
                      singleCard: false,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
