import 'package:auto_size_text/auto_size_text.dart';
import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/responsive_size.dart';
import '../../../model/cart_model.dart';

class CourseList extends StatelessWidget {
  const CourseList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "All Courses",
            style: TextStyle(
              fontSize: getTextSize(17),
              fontWeight: FontWeight.w600,
              color: kDarkText,
            ),
          ),
          getVerticalSpace(15),
          ...List.generate(
              featuredList.length,
              (index) => ListItem(
                    item: featuredList[index],
                  ))
        ],
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  const ListItem({
    Key? key,
    this.item,
  }) : super(key: key);
  final CartModel? item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: getScreenHeight(80),
            width: getScreeWidth(80),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                item!.imgUrl!,
                fit: BoxFit.cover,
              ),
            ),
          ),
          getHorizontalSpace(15),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  item!.title!,
                  maxLines: 2,
                  minFontSize: 12,
                  maxFontSize: 15,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: getTextSize(15),
                    fontWeight: FontWeight.w600,
                    color: kDarkText,
                  ),
                ),
                Text(
                  "Json Andy",
                  style: TextStyle(
                    fontSize: getTextSize(13),
                    fontWeight: FontWeight.w400,
                    color: kLightGreyColor,
                  ),
                ),
                getVerticalSpace(5),
                RatingBar.builder(
                  itemSize: 15,
                  initialRating: 3,
                  minRating: 0,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: const EdgeInsets.only(right: 4.0),
                  itemBuilder: (context, _) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                    size: 10,
                  ),
                  onRatingUpdate: (rating) {
                    debugPrint(rating.toString());
                  },
                ),
                getVerticalSpace(5),
                Text(
                  "\$${item!.price}",
                  style: TextStyle(
                    fontSize: getTextSize(17),
                    fontWeight: FontWeight.w500,
                    color: kBlueColor,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
