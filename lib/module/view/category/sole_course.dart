import 'package:ellen_learning_app/module/view/category/components/course_list.dart';
import 'package:ellen_learning_app/module/view/category/components/top_instructor.dart';
import 'package:ellen_learning_app/widgets/appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import 'components/feature_course.dart';

class SingleCourse extends StatelessWidget {
  const SingleCourse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final courseName = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: Text(courseName),
        actions: [
          const CartBadge(),
          getHorizontalSpace(20),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 30),
        child: Column(
          children: [
            FeatureCourse(course: courseName),
            TopInstructor(course: courseName),
            const CourseList(),
          ],
        ),
      ),
    );
  }
}
