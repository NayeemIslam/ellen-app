import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:ellen_learning_app/module/view/account/account.dart';
import 'package:ellen_learning_app/module/view/category/category_page.dart';
import 'package:ellen_learning_app/module/view/wishlist/wishlist.dart';
import 'package:ellen_learning_app/widgets/appbar_widget.dart';
import 'package:ellen_learning_app/widgets/home_header.dart';
import 'package:ellen_learning_app/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../constants/assets_path.dart';
import '../../constants/responsive_size.dart';
import '../../widgets/bottom_nav.dart';
import 'courses/my_courses.dart';
import 'home/home_page.dart';

class LandingPage extends StatelessWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
        init: AppController(),
        builder: (appCtrl) {
          return Scaffold(
            endDrawer: const MenuDrawer(),
            resizeToAvoidBottomInset: true,
            endDrawerEnableOpenDragGesture: false,
            appBar: AppBar(
              toolbarHeight: appCtrl.bottomIndex.value == 0 ? 80 : 60,
              flexibleSpace: appCtrl.bottomIndex.value == 0
                  ? const SafeArea(child: HomeHeader())
                  : SafeArea(child: buildAppBarWidget(appCtrl)),
              actions: [
                CartBadge(top: appCtrl.bottomIndex.value == 0 ? 15 : 5),
                getHorizontalSpace(10),
                appCtrl.bottomIndex.value == 0
                    ? Builder(builder: (context) {
                        return IconButton(
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            Scaffold.of(context).openEndDrawer();
                          },
                          icon: SvgPicture.asset(
                            menu,
                            height: getScreeWidth(24),
                          ),
                        );
                      })
                    : const SizedBox(),
                getHorizontalSpace(10),
              ],
            ),
            body: SafeArea(
              child: LayoutBuilder(builder: (context, constraints) {
                return SizedBox(
                  height: constraints.maxHeight,
                  width: constraints.maxWidth,
                  child: PageView.builder(
                    controller: appCtrl.pageViewCtrl,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: 5,
                    itemBuilder: (ctx, index) => pageTransit(index),
                  ),
                );
              }),
            ),
            bottomSheet: CustomBottomNavBar(),
          );
        });
  }

  Widget buildAppBarWidget(AppController app) {
    switch (app.bottomIndex.value) {
      case 1:
        return const AppBarWidget(title: "Category");
      case 2:
        return const AppBarWidget(title: "My Course");
      case 3:
        return const AppBarWidget(title: "Wishlist");
      case 4:
        return const AppBarWidget(title: "Account", icon: false);
      default:
        return const SizedBox();
    }
  }

  pageTransit(int index) {
    switch (index) {
      case 0:
        return const HomePage();
      case 1:
        return const CategoryPage();
      case 2:
        return const MyCourses();
      case 3:
        return const FavoritePage();
      case 4:
        return const AccountPage();
      default:
        return const SizedBox();
    }
  }
}
