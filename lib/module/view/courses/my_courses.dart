import 'package:auto_size_text/auto_size_text.dart';
import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:ellen_learning_app/module/model/cart_model.dart';
import 'package:flutter/material.dart';

import '../../../constants/app_color.dart';
import '../../../constants/responsive_size.dart';

class MyCourses extends StatelessWidget {
  const MyCourses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        itemCount: 2,
        itemBuilder: (context, index) {
          return MyCourseItem(
            item: featuredList[index],
            progressValue: 0.5 * (index + 0.2),
          );
        });
  }
}

class MyCourseItem extends StatelessWidget {
  const MyCourseItem({Key? key, @required this.item, this.progressValue = 0.5})
      : super(key: key);
  final CartModel? item;
  final double? progressValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: getScreenHeight(80),
            width: getScreeWidth(80),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                item!.imgUrl!,
                fit: BoxFit.cover,
              ),
            ),
          ),
          getHorizontalSpace(15),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  item!.title!,
                  maxLines: 2,
                  minFontSize: 12,
                  maxFontSize: 15,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: getTextSize(15),
                    fontWeight: FontWeight.w600,
                    color: kDarkText,
                  ),
                ),
                Text(
                  "Json Andy",
                  style: TextStyle(
                    fontSize: getTextSize(13),
                    fontWeight: FontWeight.w400,
                    color: kLightGreyColor,
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: LinearProgressIndicator(
                      value: progressValue,
                      semanticsLabel: 'Linear progress indicator',
                      valueColor:
                          const AlwaysStoppedAnimation<Color>(kBlueColor),
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
                RichText(
                    text: TextSpan(
                        text: "${progressValue! * 100}% ",
                        style: TextStyle(
                          fontSize: getTextSize(13),
                          fontWeight: FontWeight.w400,
                          color: kNavyBlueColor,
                        ),
                        children: [
                      TextSpan(
                        text: "Completed",
                        style: TextStyle(
                          fontSize: getTextSize(13),
                          fontWeight: FontWeight.w400,
                          color: kLightGreyColor,
                        ),
                      )
                    ]))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
