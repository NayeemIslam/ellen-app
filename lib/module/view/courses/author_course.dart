import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_color.dart';
import '../../../constants/app_constants.dart';
import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import '../details/components/instructor.dart';

class AuthorCourses extends StatelessWidget {
  const AuthorCourses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: const Text("My Courses"),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        children: [
          ...List.generate(
            featuredList.length,
            (index) => Container(
              margin: const EdgeInsets.only(bottom: 15.0),
              height: getScreenHeight(80),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: getScreenHeight(80),
                    width: getScreeWidth(80),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.asset(
                        featuredList[index].imgUrl!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  getHorizontalSpace(15),
                  Expanded(
                    child: Column(
                      // mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText(
                          featuredList[index].title!,
                          maxLines: 2,
                          minFontSize: 12,
                          maxFontSize: 15,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: getTextSize(15),
                            fontWeight: FontWeight.w600,
                            color: kNavyBlueColor,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const StudentNumber(),
                            InkWell(
                              onTap: () {
                                bottomSheet(context);
                              },
                              child: const Icon(
                                Icons.more_vert,
                                size: 20,
                                color: kGreyIconColor,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void bottomSheet(BuildContext context) {
    showModalBottomSheet(
        isDismissible: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(30),
          ),
        ),
        context: context,
        builder: (context) {
          return SizedBox(
            height: getScreenHeight(330),
            child: Column(
              children: [
                Material(
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(30)),
                  elevation: 0.5,
                  child: Container(
                    height: getScreenHeight(76),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Course Options",
                          style: TextStyle(
                            fontSize: getTextSize(18),
                            fontWeight: FontWeight.w500,
                            color: kDarkText,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Get.back();
                          },
                          child: const Icon(
                            Icons.clear,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ...List.generate(
                    courseOption.length,
                    (index) => Flexible(
                          child: ListTile(
                            onTap: () {},
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 0),
                            title: Text(
                              courseOption[index],
                              style: TextStyle(
                                fontSize: getTextSize(15),
                                fontWeight: FontWeight.w400,
                                color: kDarkText,
                              ),
                            ),
                            trailing: const Icon(
                              Icons.arrow_forward_ios,
                              size: 15,
                            ),
                          ),
                        ))
              ],
            ),
          );
        });
  }
}
