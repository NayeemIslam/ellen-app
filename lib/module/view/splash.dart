import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/app_text.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../config/route/app_route.dart';
import '../../constants/responsive_size.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(3.seconds)
        .then((value) => Get.offAndToNamed(AppRoute.landPage));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LayoutBuilder(
        builder: (ctx, constraints) {
          return OrientationBuilder(builder: (ctx, orientation) {
            ResponsiveSize.init(ctx, orientation);
            return SafeArea(
              child: SizedBox(
                height: constraints.maxHeight,
                width: constraints.maxWidth,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(splashUpper),
                    Expanded(
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          getVerticalSpace(45),
                          Text(
                            appName,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: getTextSize(70),
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.3,
                              height: 1.5,
                              color: kNavyBlueColor,
                            ),
                          ),
                          getVerticalSpace(10),
                          Text(
                            splashText,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: getTextSize(16),
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.3,
                              height: 1.5,
                              color: kLightGreyColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Image.asset(splashLower),
                  ],
                ),
              ),
            );
          });
        },
      ),
    );
  }
}
