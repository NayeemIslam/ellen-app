import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/app_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';

class BlogDetail extends StatelessWidget {
  const BlogDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: const Text("Blog"),
      ),
      body: ListView(padding: const EdgeInsets.only(bottom: 20), children: [
        Image.asset(
          "assets/images/09.jpg",
          fit: BoxFit.fill,
        ),
        getVerticalSpace(15),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  FittedBox(
                    child: Text(
                      "EnvyTheme",
                      style: TextStyle(
                        fontSize: getTextSize(12),
                        color: kGreyIconColor,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  getHorizontalSpace(8),
                  const CircleAvatar(radius: 4, backgroundColor: kBlueColor),
                  getHorizontalSpace(8),
                  FittedBox(
                    child: Text(
                      "July 10, 2020",
                      style: TextStyle(
                        fontSize: getTextSize(12),
                        color: kLightTextColor,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  getHorizontalSpace(8),
                  const CircleAvatar(radius: 4, backgroundColor: kBlueColor),
                  getHorizontalSpace(8),
                  FittedBox(
                    child: Text(
                      "3 Mins Read",
                      style: TextStyle(
                        fontSize: getTextSize(12),
                        color: kLightTextColor,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              getVerticalSpace(20),
              Text("It’s Time To Think Differently About Homeschooling",
                  style: TextStyle(
                    fontSize: getTextSize(20),
                    fontWeight: FontWeight.w800,
                    color: kDarkText,
                  )),
              getVerticalSpace(20),
              Text(
                loremText + "\n\n" + loremText,
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w400,
                  color: kLightTextColor,
                  height: 1.5,
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: const EdgeInsets.symmetric(vertical: 20),
                height: getScreenHeight(144),
                width: getScreeWidth(335),
                decoration: const BoxDecoration(
                  // borderRadius: BorderRadius.circular(4),
                  color: Colors.white,
                  border: Border(
                    left: BorderSide(
                      color: kBlueColor,
                      width: 2,
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
                child: Text(
                  blogText,
                  style: TextStyle(
                    fontSize: getTextSize(16),
                    fontWeight: FontWeight.w600,
                    color: kDarkText,
                    height: 1.5,
                  ),
                ),
              ),
              Text(
                loremText,
                style: TextStyle(
                  fontSize: getTextSize(15),
                  fontWeight: FontWeight.w400,
                  color: kLightTextColor,
                  height: 1.5,
                ),
              ),
              getVerticalSpace(25),
              Text(
                "Four Major Elements That We Offer",
                style: TextStyle(
                  fontSize: getTextSize(20),
                  fontWeight: FontWeight.w800,
                  color: kDarkText,
                ),
              ),
              getVerticalSpace(15),
              ...List.generate(
                  majorElement.length,
                  (index) => Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 5,
                              backgroundColor: kLightTextColor.withOpacity(0.6),
                            ),
                            getHorizontalSpace(10),
                            Text(
                              majorElement[index],
                              style: TextStyle(
                                fontSize: getTextSize(15),
                                fontWeight: FontWeight.w400,
                                color: kLightTextColor,
                              ),
                            ),
                          ],
                        ),
                      ))
            ],
          ),
        ),
      ]),
    );
  }
}
