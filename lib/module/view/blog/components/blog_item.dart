import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../config/route/app_route.dart';
import '../../../../constants/app_text.dart';
import '../../../../constants/responsive_size.dart';

class BlogItem extends StatelessWidget {
  const BlogItem({Key? key, this.index}) : super(key: key);
  final int? index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(AppRoute.blogDetails);
      },
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Image.asset(
              blogList[index!]["img"]!,
              height: getScreenHeight(80),
              width: getScreeWidth(80),
              fit: BoxFit.cover,
            ),
          ),
          getHorizontalSpace(15),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  blogList[index!]["title"]!,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: getTextSize(14),
                    color: kDarkText,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                getVerticalSpace(10),
                Row(
                  children: [
                    FittedBox(
                      child: Text(
                        "Json Andy",
                        style: TextStyle(
                          fontSize: getTextSize(12),
                          color: kLightTextColor,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    getHorizontalSpace(8),
                    const CircleAvatar(radius: 3, backgroundColor: kBlueColor),
                    getHorizontalSpace(8),
                    FittedBox(
                      child: Text(
                        blogList[index!]["date"]!,
                        style: TextStyle(
                          fontSize: getTextSize(12),
                          color: kLightTextColor,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
