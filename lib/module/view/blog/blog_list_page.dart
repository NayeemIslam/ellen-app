import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../constants/app_constants.dart';
import '../../../constants/app_text.dart';
import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import 'components/blog_item.dart';

class BlogListPage extends StatelessWidget {
  const BlogListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: SvgPicture.asset(back),
        ),
        title: const Text("Blog Details"),
      ),
      body: LayoutBuilder(
        builder: (ctx, constraints) {
          return Container(
            height: constraints.maxHeight,
            width: constraints.maxWidth,
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  // controller: searchController,
                  expands: false,
                  decoration:
                      textInputDecoration(hint: "Search Article").copyWith(
                    prefixIcon: IconButton(
                      onPressed: () {
                        // appCtrl.addTopSearch(searchController.text);
                        // searchController.clear();
                      },
                      icon: SvgPicture.asset(search, color: kBlueColor),
                    ),
                  ),
                ),
                getVerticalSpace(15),
                Expanded(
                  child: ListView.separated(
                    itemCount: blogList.length,
                    separatorBuilder: (ctx, index) => const Divider(),
                    itemBuilder: (ctx, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: BlogItem(
                          index: index,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
