import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_text.dart';
import '../../../constants/responsive_size.dart';

class TermsAndConPage extends StatelessWidget {
  const TermsAndConPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   leading: IconButton(
        //     onPressed: () => Get.back(),
        //     icon: SvgPicture.asset(
        //       back,
        //       // height: getScreeWidth(24),
        //       // width: getScreeWidth(24),
        //     ),
        //   ),
        //   title: const Text("Privacy Policy"),
        // ),
        body: CustomScrollView(
      slivers: [
        SliverAppBar(
          titleSpacing: 0,
          leading: IconButton(
            onPressed: () => Get.back(),
            icon: SvgPicture.asset(
              back,
              // height: getScreeWidth(24),
              // width: getScreeWidth(24),
            ),
          ),
          expandedHeight: ResponsiveSize.screenHeight * 0.3,
          floating: true,
          pinned: true,
          snap: true,
          flexibleSpace: FlexibleSpaceBar(
            expandedTitleScale: 1,
            // stretchModes: [StretchMode.zoomBackground],
            title: const Text(
              "Terms And Conditions",
              style: TextStyle(
                color: kNavyBlueColor,
              ),
            ),
            background: Image.asset(
              tcImage,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  getVerticalSpace(10),
                  TextHeader(
                    text: "${index + 1}. ${privacyHeader[index]}",
                    fontSize: 16,
                  ),
                  getVerticalSpace(10),
                  Text(
                    privacyDummyText,
                    style: TextStyle(
                      color: kLightTextColor,
                      fontWeight: FontWeight.w400,
                      fontSize: getTextSize(14),
                      height: 1.5,
                    ),
                  ),
                ],
              ),
            );
          },
          childCount: privacyHeader.length,
        )),
      ],
    ));
  }
}

class TextHeader extends StatelessWidget {
  const TextHeader(
      {Key? key, this.text, @required this.fontSize, this.textAlign})
      : super(key: key);
  final String? text;
  final double? fontSize;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      text!,
      textAlign: textAlign,
      style: TextStyle(
        fontSize: getTextSize(fontSize!),
        fontWeight: FontWeight.w800,
        color: kDarkText,
      ),
    );
  }
}
