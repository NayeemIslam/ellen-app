import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/app_text.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/responsive_size.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: SvgPicture.asset(
            back,
            // height: getScreeWidth(24),
            // width: getScreeWidth(24),
          ),
        ),
        title: const Text("About"),
      ),
      body: LayoutBuilder(
        builder: (ctx, constraints) {
          return ListView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.asset(about1),
              ),
              const TextHeader(text: "About Us"),
              const DummyText(
                text: loremText + loremText,
              ),
              const TextHeader(text: "Our Mission"),
              const DummyText(text: loremText),
              getVerticalSpace(20),
              Row(
                children: [
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(6),
                      child: Image.asset(about2),
                    ),
                  ),
                  getHorizontalSpace(10),
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(6),
                      child: Image.asset(about3),
                    ),
                  ),
                ],
              ),
              getVerticalSpace(20),
              const DummyText(text: loremText),
              const TextHeader(text: "Our Vission"),
              const DummyText(text: loremText),
            ],
          );
        },
      ),
    );
  }
}

class DummyText extends StatelessWidget {
  const DummyText({
    Key? key,
    this.text,
  }) : super(key: key);
  final String? text;
  @override
  Widget build(BuildContext context) {
    return Text(
      text!,
      style: TextStyle(
        fontSize: getTextSize(15),
        fontWeight: FontWeight.w400,
        color: kLightTextColor,
        height: 1.5,
        letterSpacing: 0.3,
      ),
    );
  }
}

class TextHeader extends StatelessWidget {
  const TextHeader({
    Key? key,
    this.text,
  }) : super(key: key);
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 15),
      child: Text(
        text!,
        style: TextStyle(
          fontSize: getTextSize(17),
          fontWeight: FontWeight.w700,
          color: kDarkText,
        ),
      ),
    );
  }
}
