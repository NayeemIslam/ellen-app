import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:ellen_learning_app/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AccountPage extends StatelessWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController app = Get.find();
    if (app.userExists.value) {
      return const Profile();
    } else {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: CustomButton(
            title: "Try to login",
            press: () {
              Get.toNamed(AppRoute.loginPage);
            },
          ),
        ),
      );
    }
  }
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        // padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        children: [
          Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            // fit: StackFit.loose,
            children: const [
              CircleAvatar(
                radius: 70,
                backgroundColor: Colors.transparent,
                backgroundImage: AssetImage(
                  "assets/images/36.jpg",
                ),
              ),
              Positioned(
                right: 0,
                bottom: 10,
                child: CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.edit,
                    color: kDarkText,
                    size: 15,
                  ),
                ),
              )
            ],
          ),
          getVerticalSpace(15),
          Text(
            "John Anderson",
            style: TextStyle(
              fontSize: getTextSize(24),
              fontWeight: FontWeight.w700,
              color: kDarkText,
            ),
          ),
          getVerticalSpace(8),
          Text(
            "anderson1995@gmail.com",
            style: TextStyle(
              fontSize: getTextSize(14),
              fontWeight: FontWeight.w400,
              color: kGreyIconColor,
            ),
          ),
          getVerticalSpace(20),
          GestureDetector(
            onTap: () => Get.toNamed(AppRoute.instructorPage),
            child: Text(
              "Switch to instructor view",
              style: TextStyle(
                fontSize: getTextSize(15),
                fontWeight: FontWeight.w600,
                color: kBlueColor,
              ),
            ),
          ),
          getVerticalSpace(50),
          Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              tilePadding: EdgeInsets.zero,
              childrenPadding: EdgeInsets.zero,
              title: const Text("Account Information"),
              initiallyExpanded: true,
              textColor: kDarkText,
              iconColor: kGreyIconColor,
              children: [
                ...List.generate(
                  3,
                  (index) => ListTile(
                    contentPadding: EdgeInsets.zero,
                    title: Text(
                      "Phone",
                      style: TextStyle(
                        fontSize: getTextSize(14),
                        fontWeight: FontWeight.w500,
                        color: kGreyIconColor,
                      ),
                    ),
                    trailing: Text(
                      "+00 990 249 3425",
                      style: TextStyle(
                        fontSize: getTextSize(14),
                        fontWeight: FontWeight.w500,
                        color: kDarkText,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          // getVerticalSpace(15),
          Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              tilePadding: EdgeInsets.zero,
              childrenPadding: EdgeInsets.zero,
              title: const Text("Video Playback Options"),
              initiallyExpanded: true,
              textColor: kDarkText,
              iconColor: kGreyIconColor,
              children: [
                ...List.generate(
                  2,
                  (index) => ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        "Play audio in background",
                        style: TextStyle(
                          fontSize: getTextSize(14),
                          fontWeight: FontWeight.w500,
                          color: kGreyIconColor,
                        ),
                      ),
                      trailing: Switch(
                        value: false,
                        onChanged: (v) {
                          debugPrint(v.toString());
                        },
                      )),
                )
              ],
            ),
          ),

          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Share This App",
              style: TextStyle(
                fontSize: getTextSize(15),
                fontWeight: FontWeight.w500,
                color: kDarkText,
              ),
            ),
            trailing: const Icon(
              Icons.arrow_forward_ios,
              size: 15,
              color: kGreyIconColor,
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            minVerticalPadding: 0,
            title: Text(
              "Close The Account",
              style: TextStyle(
                fontSize: getTextSize(15),
                fontWeight: FontWeight.w500,
                color: kDarkText,
              ),
            ),
            trailing: const Icon(
              Icons.arrow_forward_ios,
              size: 15,
              color: kGreyIconColor,
            ),
          ),
        ],
      ),
    );
  }
}
