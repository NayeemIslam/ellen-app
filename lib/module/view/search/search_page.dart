import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_color.dart';
import '../../../constants/app_constants.dart';
import '../../../constants/assets_path.dart';
import '../../../constants/responsive_size.dart';
import '../../controller/app_controller.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController searchController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kBgColor,
          elevation: 0,
          automaticallyImplyLeading: true,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: SvgPicture.asset(back),
          ),
          toolbarHeight: 70,
          title: Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: TextField(
              controller: searchController,
              expands: false,
              decoration:
                  textInputDecoration(hint: "Search your Course...").copyWith(
                suffixIcon: InkWell(
                  onTap: () {
                    appCtrl.addTopSearch(searchController.text);
                    searchController.clear();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: kBlueColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    margin: const EdgeInsets.all(5),
                    alignment: Alignment.center,
                    height: getScreeWidth(45),
                    width: getScreeWidth(45),
                    child: SvgPicture.asset(
                      search,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        body: LayoutBuilder(builder: (context, constraints) {
          return SizedBox(
            height: constraints.maxHeight,
            width: constraints.maxWidth,
            child: SingleChildScrollView(
              child: GetBuilder<AppController>(
                init: AppController(),
                builder: (appCtrl) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getVerticalSpace(40),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Top Searches",
                          style: TextStyle(
                            fontSize: getTextSize(18),
                            fontWeight: FontWeight.w600,
                            color: kDarkText,
                          ),
                        ),
                      ),
                      // getVerticalSpace(20),
                      if (appCtrl.topSearch.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Wrap(
                            runSpacing: 10,
                            spacing: 5,
                            children: List.generate(
                              appCtrl.topSearch.length,
                              (index) {
                                final textLength =
                                    appCtrl.topSearch[index].length;
                                return Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 5),
                                  height: getScreenHeight(45),
                                  width: getScreeWidth(textLength > 6
                                      ? (textLength * 12)
                                      : (textLength * 18)),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(36),
                                      boxShadow: [
                                        BoxShadow(
                                          offset: const Offset(0, 2),
                                          blurRadius: 8,
                                          color: Colors.black.withOpacity(0.07),
                                        )
                                      ]),
                                  child: Text(
                                    appCtrl.topSearch[index],
                                    style: TextStyle(
                                      fontSize: getTextSize(14),
                                      color: kNavyBlueColor,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      getVerticalSpace(20),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Top Categories",
                          style: TextStyle(
                            fontSize: getTextSize(18),
                            fontWeight: FontWeight.w600,
                            color: kDarkText,
                          ),
                        ),
                      ),

                      ...List.generate(
                        topcategoriesList.length,
                        (index) => Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                topcategoriesList[index]['title'],
                                style: TextStyle(
                                  fontSize: getTextSize(14),
                                  fontWeight: FontWeight.w400,
                                  color: kNavyBlueColor,
                                ),
                              ),
                              Text(
                                "(${topcategoriesList[index]['total']})",
                                style: TextStyle(
                                  fontSize: getTextSize(14),
                                  fontWeight: FontWeight.w400,
                                  color: kNavyBlueColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          );
        }),
      ),
    );
  }
}
