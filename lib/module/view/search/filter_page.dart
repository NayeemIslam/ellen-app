import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/module/view/search/components/price.dart';
import 'package:ellen_learning_app/module/view/search/components/ratings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../constants/responsive_size.dart';
import '../../../widgets/custom_button.dart';
import 'components/level.dart';
import 'components/sort_by.dart';

class FilterPage extends StatelessWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // AppController appCtrl = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: SvgPicture.asset(
            back,
            // height: getScreeWidth(24),
            // width: getScreeWidth(24),
          ),
        ),
        title: const Text("Filters"),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SortFields(),
            const LevelFields(),
            const PriceFields(),
            const RatingsFields(),
            getVerticalSpace(30),
            CustomButton(
              title: "Apply Filter",
              press: () {},
            ),
          ],
        ),
      ),
    );
  }
}
