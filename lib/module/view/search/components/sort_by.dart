import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/responsive_size.dart';
import '../../../controller/app_controller.dart';

class SortFields extends StatelessWidget {
  const SortFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Sort By",
          style: TextStyle(
            fontSize: getTextSize(16),
            fontWeight: FontWeight.w600,
            color: kDarkText,
          ),
        ),
        getVerticalSpace(15),
        DecoratedBox(
          decoration: const ShapeDecoration(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                  width: 1.0, color: Colors.white, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: Obx(
              () => DropdownButton<String>(
                onChanged: (v) {
                  debugPrint("V: $v");
                  appCtrl.changeSortBy(v!);
                },
                hint: Text(
                  appCtrl.sortBy.value,
                  style: const TextStyle(
                    color: kDarkText,
                    fontSize: 16,
                  ),
                ),
                underline: const SizedBox(),
                borderRadius: BorderRadius.circular(10),
                icon: const Icon(
                  Icons.keyboard_arrow_down_sharp,
                  size: 30,
                  color: kBlueColor,
                ),
                isExpanded: true,
                items: filterItems
                    .asMap()
                    .entries
                    .map((entry) => DropdownMenuItem<String>(
                          onTap: () {
                            debugPrint(appCtrl.sortBy.value);
                          },
                          value: filterItems[entry.key],
                          enabled: true,
                          child: Row(
                            children: [
                              Text(filterItems[entry.key].toString()),
                            ],
                          ),
                        ))
                    .toList(),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
