import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/app_constants.dart';
import '../../../../constants/responsive_size.dart';
import '../../../controller/app_controller.dart';

class PriceFields extends StatelessWidget {
  const PriceFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        getVerticalSpace(30),
        Text(
          "Price",
          style: TextStyle(
            fontSize: getTextSize(16),
            fontWeight: FontWeight.w600,
            color: kDarkText,
          ),
        ),
        getVerticalSpace(15),
        ...List.generate(
            priceList.length,
            (index) => Obx(
                  () => InkWell(
                    onTap: () {
                      appCtrl.changePriceBy(index);
                    },
                    child: AnimatedContainer(
                      duration: 500.milliseconds,
                      curve: Curves.ease,
                      height: getScreenHeight(60),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          )),
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      margin: const EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            priceList[index],
                            style: TextStyle(
                              color: appCtrl.priceBy.value == index
                                  ? kDarkText
                                  : kLightTextColor,
                              fontSize: 14,
                            ),
                          ),
                          CircleAvatar(
                            radius: 12,
                            backgroundColor: appCtrl.priceBy.value == index
                                ? kBlueColor
                                : const Color(0xffDBDCDF),
                            child: const Icon(
                              Icons.check,
                              color: Colors.white,
                              size: 15,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
      ],
    );
  }
}
