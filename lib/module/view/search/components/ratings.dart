import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/app_constants.dart';
import '../../../../constants/responsive_size.dart';
import '../../../controller/app_controller.dart';

class RatingsFields extends StatelessWidget {
  const RatingsFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        getVerticalSpace(30),
        Text(
          "Ratings",
          style: TextStyle(
            fontSize: getTextSize(16),
            fontWeight: FontWeight.w600,
            color: kDarkText,
          ),
        ),
        getVerticalSpace(15),
        ...List.generate(
            ratingsList.length,
            (index) => Obx(
                  () => InkWell(
                    onTap: () {
                      appCtrl.changeRatingsBy(index);
                    },
                    child: AnimatedContainer(
                      duration: 500.milliseconds,
                      curve: Curves.ease,
                      height: getScreenHeight(60),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          )),
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      margin: const EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            ratingsList[index],
                            style: TextStyle(
                              color: appCtrl.ratingsBy.value == index
                                  ? kDarkText
                                  : kLightTextColor,
                              fontSize: 14,
                            ),
                          ),
                          CircleAvatar(
                            radius: 12,
                            backgroundColor: appCtrl.ratingsBy.value == index
                                ? kBlueColor
                                : const Color(0xffDBDCDF),
                            child: const Icon(
                              Icons.check,
                              color: Colors.white,
                              size: 15,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
      ],
    );
  }
}
