import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:ellen_learning_app/module/view/home/components/featured_course.dart';
import 'package:ellen_learning_app/module/view/home/components/students_view.dart';
import 'package:ellen_learning_app/module/view/home/components/tab_bar.dart';
import 'package:ellen_learning_app/module/view/home/components/top_category.dart';
import 'package:ellen_learning_app/module/view/home/components/top_it_course.dart';
import 'package:ellen_learning_app/module/view/home/components/trend_course.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController app = Get.find();

    return ListView(
      // crossAxisAlignment: CrossAxisAlignment.start,
      padding: const EdgeInsets.symmetric(vertical: 20),
      controller: app.scrollController,
      physics: app.physics,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            children: [
              Expanded(
                child: InkWell(
                  // onTap: _showSearch,
                  onTap: () {
                    Get.toNamed(AppRoute.searchPage);
                  },
                  child: Container(
                    height: getScreenHeight(50),
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        SvgPicture.asset(search),
                        getHorizontalSpace(10),
                        const Text(
                          "Search Your Course",
                          style: TextStyle(
                            fontSize: 14,
                            color: kLightTextColor,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              getHorizontalSpace(10),
              InkWell(
                onTap: () {
                  Get.toNamed(AppRoute.filterPage);
                },
                child: Container(
                  height: getScreenHeight(50),
                  width: getScreeWidth(50),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: SvgPicture.asset(
                    filter,
                    color: kBlueColor,
                  ),
                ),
              ),
            ],
          ),
        ),
        getVerticalSpace(10),
        const TabBarCustomer(),
        const TrendingCourse(),
        const TopCategory(),
        const FeaturedCourse(),
        const StudentsView(),
        const TopItCourse(),
      ],
    );
  }
}
