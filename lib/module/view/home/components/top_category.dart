import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../widgets/title_text.dart';

class TopCategory extends StatelessWidget {
  const TopCategory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController app = Get.find();
    return Column(
      children: [
        getVerticalSpace(20),
        TitleText(
          title: "Top Categories",
          press: () {
            app.changeHomePage(1);
          },
        ),
        getVerticalSpace(10),
        SizedBox(
            height: getScreenHeight(240),
            child: GridView(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1.7),
              children: topCategories
                  .asMap()
                  .entries
                  .map(
                    (e) => CategoryCard(index: e.key),
                  )
                  .toList(),
            ))
      ],
    );
  }
}

class CategoryCard extends StatelessWidget {
  const CategoryCard({
    Key? key,
    @required this.index,
    this.height = 100,
    this.titleTextSize = 16,
  }) : super(key: key);
  final int? index;
  final double? height;
  final double? titleTextSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getScreenHeight(height!),
      // width: getScreeWidth(152),
      decoration: BoxDecoration(
        color: topCategories[index!]['color'],
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  topCategories[index!]['title'],
                  style: TextStyle(
                    fontSize: getTextSize(titleTextSize!),
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
                getVerticalSpace(6),
                Text(
                  "(${topCategories[index!]['total']})",
                  style: TextStyle(
                    fontSize: getTextSize(14),
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Image.asset(
              topCategories[index!]['avatar'],
              height: getScreeWidth(100),
            ),
          ),
        ],
      ),
    );
  }
}
