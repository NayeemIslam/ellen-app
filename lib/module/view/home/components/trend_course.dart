import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:ellen_learning_app/utils/helper/lazy_loader.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../widgets/title_text.dart';
import '../../../../widgets/trend_course_card.dart';

class TrendingCourse extends StatelessWidget {
  const TrendingCourse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DataController data = Get.find();
    return Column(
      children: [
        const TitleText(title: "Trending Courses"),
        getVerticalSpace(10),
        SizedBox(
            child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Obx(() {
            if (data.isLoading.value) {
              return Row(
                children: [
                  ...List.generate(
                    3,
                    (index) => Container(
                      margin: EdgeInsets.only(
                          right: data.courseList.length - 1 == index ? 0 : 15),
                      height: getScreenHeight(296),
                      width: getScreeWidth(200),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: getScreenHeight(160),
                            child: const ShimmerLoader(),
                          ),
                          getVerticalSpace(10),
                          SizedBox(
                            height: getScreenHeight(20),
                            child: const ShimmerLoader(),
                          ),
                          getVerticalSpace(10),
                          SizedBox(
                            height: getScreenHeight(20),
                            child: const ShimmerLoader(),
                          ),
                          getVerticalSpace(10),
                          SizedBox(
                            height: getScreenHeight(20),
                            child: const ShimmerLoader(),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              );
            } else {
              return Row(
                children: [
                  ...List.generate(
                    data.courseList.length,
                    (index) {
                      final item = data.courseList[index];
                      return Container(
                        margin: EdgeInsets.only(
                            right:
                                data.courseList.length - 1 == index ? 0 : 15),
                        height: getScreenHeight(296),
                        width: getScreeWidth(200),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: InkWell(
                          onTap: () {
                            data.fetchCourseDetails(item);
                            Get.toNamed(AppRoute.detailsPage);
                          },
                          child: TrendCard(
                            image: item.image,
                            title: item.name,
                            instructor: item.instructor!.name,
                            price: item.price,
                          ),
                        ),
                      );
                    },
                  )
                ],
              );
            }
          }),
        )),
      ],
    );
  }
}
