import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/app_color.dart';
import '../../../../constants/app_constants.dart';
import '../../../../constants/responsive_size.dart';
import '../../../controller/app_controller.dart';

class TabBarCustomer extends StatelessWidget {
  const TabBarCustomer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Obx(() {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SizedBox(
          height: getScreenHeight(70),
          child: Row(
            children: tabItemsList
                .asMap()
                .entries
                .map((e) => InkWell(
                      radius: 0,
                      onTap: () {
                        appCtrl.changeTab(e.key);
                      },
                      child: AnimatedContainer(
                        height: getScreenHeight(45),
                        duration: 300.milliseconds,
                        curve: Curves.ease,
                        margin: EdgeInsets.only(
                            right: tabItemsList.length - 1 == e.key ? 0 : 10),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 5),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(36),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (appCtrl.tabBarIndex.value == e.key)
                              const Padding(
                                padding: EdgeInsets.only(right: 8),
                                child: CircleAvatar(
                                    radius: 4, backgroundColor: kBlueColor),
                              ),
                            Text(
                              tabItemsList[e.key],
                              style: TextStyle(
                                fontSize: getTextSize(14),
                                fontWeight: appCtrl.tabBarIndex.value == e.key
                                    ? FontWeight.w600
                                    : FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      );
    });
  }
}
