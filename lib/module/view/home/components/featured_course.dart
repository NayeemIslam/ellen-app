import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:ellen_learning_app/widgets/feature_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../widgets/title_text.dart';

class FeaturedCourse extends StatelessWidget {
  const FeaturedCourse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DataController data = Get.find();
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      margin: const EdgeInsets.only(top: 40),
      child: Column(
        children: [
          getVerticalSpace(20),
          const TitleText(title: "Featured Courses"),
          getVerticalSpace(10),
          SizedBox(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Obx(() {
                return Row(
                  children: [
                    ...List.generate(
                      data.courseList.length,
                      (index) {
                        final item = data.courseList[index];
                        return Container(
                          margin: EdgeInsets.only(
                              right: data.courseList.length - 1 == index ? 0 : 15),
                          height: getScreenHeight(243),
                          width: getScreeWidth(186),
                          child: InkWell(
                            onTap: () {
                              // cart.showDetails(item);
                              // Get.toNamed(AppRoute.detailsPage);
                            },
                            child: FeatureCard(
                                image: item.image,
                                title: item.name,
                                instructor: item.instructor!.name,
                                price: item.price),
                          ),
                        );
                      },
                    )
                  ],
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
