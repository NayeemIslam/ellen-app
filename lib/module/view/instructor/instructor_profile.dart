import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/module/controller/data_controller.dart';
import 'package:ellen_learning_app/module/view/category/components/course_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_constants.dart';
import '../../../constants/assets_path.dart';
import '../details/components/instructor.dart';

class InstructorProfile extends StatelessWidget {
  const InstructorProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DataController data = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () => Get.back(), icon: SvgPicture.asset(back)),
        title: const Text("Instructor"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const InstructorCard(
                    radius: 40,
                    titleTextSize: 17,
                    singleCard: true,
                  ),
                  getVerticalSpace(20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      DataItems(
                        title: "Courses",
                        value: 350,
                      ),
                      DataItems(
                        title: "Total Students",
                        value: 350243,
                      ),
                      DataItems(
                        title: "Reviews",
                        value: 154411,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            getVerticalSpace(30),
            Container(
              padding: const EdgeInsets.all(20),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "About Me",
                    style: TextStyle(
                      fontSize: getTextSize(17),
                      fontWeight: FontWeight.w600,
                      color: kNavyBlueColor,
                    ),
                  ),
                  getVerticalSpace(10),
                  Text(
                    data.instructorInfo.value.description!,
                    style: TextStyle(
                      fontSize: getTextSize(14),
                      fontWeight: FontWeight.w400,
                      color: kGreyIconColor,
                      height: 1.5,
                      letterSpacing: 0.3,
                    ),
                  ),
                  getVerticalSpace(40),
                  Text(
                    "My Courses (12)",
                    style: TextStyle(
                      fontSize: getTextSize(17),
                      fontWeight: FontWeight.w600,
                      color: kNavyBlueColor,
                    ),
                  ),
                  getVerticalSpace(15),
                  ...List.generate(
                    3,
                    (index) => ListItem(
                      item: featuredList[index],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DataItems extends StatelessWidget {
  const DataItems({
    Key? key,
    @required this.title,
    @required this.value,
  }) : super(key: key);
  final String? title;
  final int? value;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          title!,
          style: TextStyle(
            fontSize: getTextSize(12),
            fontWeight: FontWeight.w400,
            color: kGreyIconColor,
          ),
        ),
        getVerticalSpace(5),
        Text(
          "$value+",
          style: TextStyle(
            fontSize: getTextSize(18),
            fontWeight: FontWeight.w400,
            color: kDarkText,
          ),
        )
      ],
    );
  }
}
