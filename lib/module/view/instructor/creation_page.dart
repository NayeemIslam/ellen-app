import 'package:ellen_learning_app/config/route/app_route.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_color.dart';
import '../../../constants/app_text.dart';
import '../../../constants/responsive_size.dart';

class CreationPage extends StatelessWidget {
  const CreationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () => Get.back(), icon: SvgPicture.asset(back)),
        title: const Text("Become An Instructor"),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          children: [
            Image.asset("assets/images/49.png"),
            getVerticalSpace(50),
            Text(
              "Jump Into Course Creation",
              style: TextStyle(
                fontSize: getTextSize(25),
                fontWeight: FontWeight.w600,
                color: kDarkText,
              ),
            ),
            getVerticalSpace(15),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child: Text(
                loremText,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: getTextSize(17),
                  fontWeight: FontWeight.w400,
                  color: kGreyIconColor,
                ),
              ),
            ),
            getVerticalSpace(50),
            CustomButton(
              title: "Create Your Course",
              press: () {
                Get.toNamed(AppRoute.submitPage);
              },
            )
          ],
        ),
      ),
    );
  }
}
