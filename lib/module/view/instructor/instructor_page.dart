import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:ellen_learning_app/constants/app_text.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/constants/responsive_size.dart';
import 'package:ellen_learning_app/widgets/appbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../config/route/app_route.dart';
import '../../../widgets/custom_button.dart';

class InstructorPage extends StatelessWidget {
  const InstructorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () => Get.back(), icon: SvgPicture.asset(back)),
        title: const Text("Become An Instructor"),
        actions: [const CartBadge(), getHorizontalSpace(20)],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset("assets/images/34.jpg"),
              ),
            ),
            getVerticalSpace(15),
            Text(
              "Come Tech With Us",
              style: TextStyle(
                fontSize: getTextSize(25),
                fontWeight: FontWeight.w600,
                color: kDarkText,
              ),
            ),
            getVerticalSpace(15),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child: Text(
                loremText,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: getTextSize(17),
                  fontWeight: FontWeight.w400,
                  color: kGreyIconColor,
                ),
              ),
            ),
            getVerticalSpace(50),
            Container(
              height: getScreenHeight(70),
              width: double.infinity,
              color: Colors.white,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Text(
                "Reasons To Start Now",
                style: TextStyle(
                  fontSize: getTextSize(20),
                  fontWeight: FontWeight.w500,
                  color: kDarkText,
                ),
              ),
            ),
            getVerticalSpace(30),
            ...List.generate(
              reasonToStart.length,
              (index) => Column(
                children: [
                  Image.asset(
                    reasonToStart[index]['avatar']!,
                  ),
                  getVerticalSpace(20),
                  Text(
                    reasonToStart[index]['title']!,
                    style: TextStyle(
                      fontSize: getTextSize(16),
                      fontWeight: FontWeight.w600,
                      color: kDarkText,
                    ),
                  ),
                  getVerticalSpace(15),
                  Text(
                    loremText,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: getTextSize(15),
                      fontWeight: FontWeight.w400,
                      color: kGreyIconColor,
                    ),
                  ),
                  getVerticalSpace(15),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        height: getScreenHeight(80),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: CustomButton(
            title: "Get Started",
            press: () {
              Get.toNamed(AppRoute.creationPage);
            }),
      ),
    );
  }
}
