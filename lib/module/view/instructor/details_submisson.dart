import 'package:ellen_learning_app/constants/app_constants.dart';
import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:ellen_learning_app/module/controller/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../constants/app_color.dart';
import '../../../constants/responsive_size.dart';

class SubmissionPage extends StatelessWidget {
  const SubmissionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () => Get.back(), icon: SvgPicture.asset(back)),
        title: const Text("Course Details Submission"),
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Course Information",
                style: TextStyle(
                  fontSize: getTextSize(14),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                ),
              ),
              getVerticalSpace(15),
              TextField(
                decoration: textInputDecoration(hint: "Add Course Title"),
              ),
              getVerticalSpace(15),
              DecoratedBox(
                decoration: const ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                        width: 1.0,
                        color: Colors.white,
                        style: BorderStyle.solid),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Obx(
                    () => DropdownButton<String>(
                      // value: "Select Category",

                      onChanged: (v) {
                        debugPrint("V: $v");
                        appCtrl.changeSortBy(v!);
                      },
                      hint: Text(
                        appCtrl.sortBy.value,
                        style: const TextStyle(
                          color: kDarkText,
                          fontSize: 16,
                        ),
                      ),
                      underline: const SizedBox(),
                      borderRadius: BorderRadius.circular(10),
                      icon: const Icon(
                        Icons.keyboard_arrow_down_sharp,
                        size: 30,
                        color: kGreyIconColor,
                      ),
                      isExpanded: true,
                      items: courseItems
                          .asMap()
                          .entries
                          .map((entry) => DropdownMenuItem<String>(
                                onTap: () {
                                  debugPrint(appCtrl.sortBy.value);
                                },
                                value: courseItems[entry.key],
                                enabled: true,
                                child: Row(
                                  children: [
                                    Text(courseItems[entry.key].toString()),
                                  ],
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                ),
              ),
              getVerticalSpace(15),
              TextField(
                maxLines: 6,
                decoration: textInputDecoration(hint: "Add Course Description"),
              ),
              getVerticalSpace(15),
              UploadField(
                title: "Upload Video File",
                press: () {},
              ),
              getVerticalSpace(15),
              TextField(
                decoration: textInputDecoration(hint: "Add Course Price"),
              ),
              getVerticalSpace(15),
              UploadField(
                title: "Upload Thumbnail",
                press: () {},
              ),
              getVerticalSpace(15),
              UploadField(
                title: "Upload Practice File",
                press: () {},
              )
            ],
          )),
    );
  }
}

class UploadField extends StatelessWidget {
  const UploadField({
    Key? key,
    @required this.title,
    this.press,
  }) : super(key: key);
  final String? title;
  final VoidCallback? press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: const ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
                width: 1.0, color: Colors.white, style: BorderStyle.solid),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title!,
              style: TextStyle(
                fontSize: getScreenHeight(14),
                fontWeight: FontWeight.w400,
                color: kHintTextColor,
              ),
            ),
            SvgPicture.asset(uploadIcon),
          ],
        ),
      ),
    );
  }
}
