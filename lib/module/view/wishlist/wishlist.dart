import 'package:ellen_learning_app/constants/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

import '../../../constants/responsive_size.dart';
import '../../controller/cart_controller.dart';
import 'components/wishlist_item.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(
        init: CartController(),
        builder: (cart) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Text(
                  "${cart.favoriteList.length} Item(s) in your favourite list"
                      .capitalize
                      .toString(),
                  style: TextStyle(
                    fontSize: getTextSize(16),
                    fontWeight: FontWeight.w600,
                    color: kGreyIconColor,
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 0),
                  itemCount: cart.favoriteList.length,
                  itemBuilder: (ctx, index) {
                    final item = cart.favoriteList[index];
                    return Slidable(
                      key: const ValueKey(0),
                      endActionPane: ActionPane(
                        motion: const BehindMotion(),
                        children: [
                          SlidableAction(
                            spacing: 2,
                            onPressed: (context) {
                              cart.removeFromFavorite(item.id);
                            },
                            // backgroundColor: const Color(0xFFFE4A49),
                            foregroundColor: Colors.black,
                            icon: Icons.remove_circle_outline,

                            label: 'Remove',
                          ),
                        ],
                      ),
                      child: FavoriteItem(item: item),
                    );
                  },
                ),
              )
            ],
          );
        });
  }
}
