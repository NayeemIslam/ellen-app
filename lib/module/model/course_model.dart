class CourseModel {
  CourseModel({
    this.id,
    this.name,
    this.slug,
    this.permalink,
    this.image,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.onSale,
    this.status,
    this.content,
    this.excerpt,
    this.duration,
    this.countStudents,
    this.canFinish,
    this.canRetake,
    this.ratakeCount,
    this.rataken,
    this.rating,
    this.price,
    this.originPrice,
    this.salePrice,
    this.instructor,
    this.sections,
  });

  CourseModel.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    permalink = json['permalink'];
    image = json['image'];
    dateCreated = json['date_created'];
    dateCreatedGmt = json['date_created_gmt'];
    dateModified = json['date_modified'];
    dateModifiedGmt = json['date_modified_gmt'];
    onSale = json['on_sale'];
    status = json['status'];
    content = json['content'];
    excerpt = json['excerpt'];
    duration = json['duration'];
    countStudents = json['count_students'];
    canFinish = json['can_finish'];
    canRetake = json['can_retake'];
    ratakeCount = json['ratake_count'];
    rataken = json['rataken'];
    rating = json['rating'];
    price = json['price'];
    originPrice = json['origin_price'];
    salePrice = json['sale_price'];
    instructor = json['instructor'] != null
        ? Instructor.fromJson(json['instructor'])
        : null;
    if (json['sections'] != null) {
      sections = [];
      json['sections'].forEach((v) {
        sections?.add(Sections.fromJson(v));
      });
    }
  }
  int? id;
  String? name;
  String? slug;
  String? permalink;
  String? image;
  String? dateCreated;
  String? dateCreatedGmt;
  String? dateModified;
  String? dateModifiedGmt;
  bool? onSale;
  String? status;
  String? content;
  String? excerpt;
  String? duration;
  int? countStudents;
  bool? canFinish;
  int? canRetake;
  int? ratakeCount;
  int? rataken;
  int? rating;
  int? price;
  int? originPrice;
  int? salePrice;
  Instructor? instructor;
  List<Sections>? sections;
  CourseModel copyWith({
    int? id,
    String? name,
    String? slug,
    String? permalink,
    String? image,
    String? dateCreated,
    String? dateCreatedGmt,
    String? dateModified,
    String? dateModifiedGmt,
    bool? onSale,
    String? status,
    String? content,
    String? excerpt,
    String? duration,
    int? countStudents,
    bool? canFinish,
    int? canRetake,
    int? ratakeCount,
    int? rataken,
    int? rating,
    int? price,
    int? originPrice,
    int? salePrice,
    Instructor? instructor,
    List<Sections>? sections,
  }) =>
      CourseModel(
        id: id ?? this.id,
        name: name ?? this.name,
        slug: slug ?? this.slug,
        permalink: permalink ?? this.permalink,
        image: image ?? this.image,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        onSale: onSale ?? this.onSale,
        status: status ?? this.status,
        content: content ?? this.content,
        excerpt: excerpt ?? this.excerpt,
        duration: duration ?? this.duration,
        countStudents: countStudents ?? this.countStudents,
        canFinish: canFinish ?? this.canFinish,
        canRetake: canRetake ?? this.canRetake,
        ratakeCount: ratakeCount ?? this.ratakeCount,
        rataken: rataken ?? this.rataken,
        rating: rating ?? this.rating,
        price: price ?? this.price,
        originPrice: originPrice ?? this.originPrice,
        salePrice: salePrice ?? this.salePrice,
        instructor: instructor ?? this.instructor,
        sections: sections ?? this.sections,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['slug'] = slug;
    map['permalink'] = permalink;
    map['image'] = image;
    map['date_created'] = dateCreated;
    map['date_created_gmt'] = dateCreatedGmt;
    map['date_modified'] = dateModified;
    map['date_modified_gmt'] = dateModifiedGmt;
    map['on_sale'] = onSale;
    map['status'] = status;
    map['content'] = content;
    map['excerpt'] = excerpt;
    map['duration'] = duration;
    map['count_students'] = countStudents;
    map['can_finish'] = canFinish;
    map['can_retake'] = canRetake;
    map['ratake_count'] = ratakeCount;
    map['rataken'] = rataken;
    map['rating'] = rating;
    map['price'] = price;
    map['origin_price'] = originPrice;
    map['sale_price'] = salePrice;
    if (instructor != null) {
      map['instructor'] = instructor?.toJson();
    }
    if (sections != null) {
      map['sections'] = sections?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Sections {
  Sections({
    this.id,
    this.title,
    this.courseId,
    this.description,
    this.order,
    this.items,
  });

  Sections.fromJson(dynamic json) {
    id = json['id'];
    title = json['title'];
    courseId = json['course_id'];
    description = json['description'];
    order = json['order'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(Items.fromJson(v));
      });
    }
  }
  String? id;
  String? title;
  int? courseId;
  String? description;
  String? order;
  List<Items>? items;
  Sections copyWith({
    String? id,
    String? title,
    int? courseId,
    String? description,
    String? order,
    List<Items>? items,
  }) =>
      Sections(
        id: id ?? this.id,
        title: title ?? this.title,
        courseId: courseId ?? this.courseId,
        description: description ?? this.description,
        order: order ?? this.order,
        items: items ?? this.items,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['title'] = title;
    map['course_id'] = courseId;
    map['description'] = description;
    map['order'] = order;
    if (items != null) {
      map['items'] = items?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Items {
  Items({
    this.id,
    this.type,
    this.title,
    this.preview,
    this.duration,
    this.graduation,
    this.status,
    this.locked,
  });

  Items.fromJson(dynamic json) {
    id = json['id'];
    type = json['type'];
    title = json['title'];
    preview = json['preview'];
    duration = json['duration'];
    graduation = json['graduation'];
    status = json['status'];
    locked = json['locked'];
  }
  int? id;
  String? type;
  String? title;
  bool? preview;
  String? duration;
  String? graduation;
  String? status;
  bool? locked;
  Items copyWith({
    int? id,
    String? type,
    String? title,
    bool? preview,
    String? duration,
    String? graduation,
    String? status,
    bool? locked,
  }) =>
      Items(
        id: id ?? this.id,
        type: type ?? this.type,
        title: title ?? this.title,
        preview: preview ?? this.preview,
        duration: duration ?? this.duration,
        graduation: graduation ?? this.graduation,
        status: status ?? this.status,
        locked: locked ?? this.locked,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['type'] = type;
    map['title'] = title;
    map['preview'] = preview;
    map['duration'] = duration;
    map['graduation'] = graduation;
    map['status'] = status;
    map['locked'] = locked;
    return map;
  }
}

class Instructor {
  Instructor({
    this.avatar,
    this.id,
    this.name,
    this.description,
    this.social,
  });

  Instructor.fromJson(dynamic json) {
    avatar = json['avatar'];
    id = json['id'];
    name = json['name'];
    description = json['description'];
    social = json['social'] != null ? Social.fromJson(json['social']) : null;
  }
  String? avatar;
  int? id;
  String? name;
  String? description;
  Social? social;
  Instructor copyWith({
    String? avatar,
    int? id,
    String? name,
    String? description,
    Social? social,
  }) =>
      Instructor(
        avatar: avatar ?? this.avatar,
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        social: social ?? this.social,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['avatar'] = avatar;
    map['id'] = id;
    map['name'] = name;
    map['description'] = description;
    if (social != null) {
      map['social'] = social?.toJson();
    }
    return map;
  }
}

class Social {
  Social({
    this.facebook,
    this.twitter,
    this.youtube,
    this.linkedin,
  });

  Social.fromJson(dynamic json) {
    facebook = json['facebook'];
    twitter = json['twitter'];
    youtube = json['youtube'];
    linkedin = json['linkedin'];
  }
  String? facebook;
  String? twitter;
  String? youtube;
  String? linkedin;
  Social copyWith({
    String? facebook,
    String? twitter,
    String? youtube,
    String? linkedin,
  }) =>
      Social(
        facebook: facebook ?? this.facebook,
        twitter: twitter ?? this.twitter,
        youtube: youtube ?? this.youtube,
        linkedin: linkedin ?? this.linkedin,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['facebook'] = facebook;
    map['twitter'] = twitter;
    map['youtube'] = youtube;
    map['linkedin'] = linkedin;
    return map;
  }
}
