class CartModel {
  String? id;
  String? imgUrl;
  String? title;
  String? instructor;
  int? price;

  CartModel({this.id, this.imgUrl, this.title, this.instructor, this.price});

  CartModel.fromMap(dynamic map) {
    id = map['id']!;
    imgUrl = map['image']!;
    title = map['title']!;
    price = map['price']!;
    instructor = map['instructor']!;
  }

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['image'] = imgUrl;
    map['title'] = title;
    map['price'] = price;
    map['instructor'] = instructor;
    return map;
  }
}
