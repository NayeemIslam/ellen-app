import 'package:ellen_learning_app/utils/helper/snack_message.dart';
import 'package:get/get.dart';

import '../model/cart_model.dart';

class CartController extends GetxController {
  var cartItemsList = <CartModel>[].obs;
  var courseDetails = CartModel();
  var vat = 2.0;
  var deliveryCost = 2.5;
  double get cartTotal =>
      cartItemsList.fold(0.0, (sum, element) => sum + element.price!);
  var cartCounter = 1.obs;

  var favoriteList = <CartModel>[].obs;

  void showDetails(CartModel value) {
    courseDetails = value;
    update();
  }

  void addToCart(CartModel cartModel) {
    var value = cartItemsList.where((e) => e.id == cartModel.id);
    var checkFav = favoriteList.where((e) => e.id == cartModel.id);
    if (value.isEmpty) {
      cartItemsList.insert(0, cartModel);
      SnackMessage().showSnackBar("Added in cart");
      if (checkFav.isNotEmpty) {
        removeFromFavorite(cartModel.id);
      }
    } else {
      SnackMessage().showSnackBar("Item exists in cart");
    }
    update();
  }

  void addToFavorite(CartModel cartModel) {
    var value = favoriteList.where((e) => e.id == cartModel.id);
    var findCartItem = cartItemsList.where((e) => e.id == cartModel.id);

    if (value.isEmpty && findCartItem.isEmpty) {
      favoriteList.insert(0, cartModel);
      SnackMessage().showSnackBar("Added in favorite");
    } else {
      if (findCartItem.isNotEmpty) {
        SnackMessage().showSnackBar("Item exists in cart");
      } else {
        SnackMessage().showSnackBar("Item exists in favorite");
      }
    }
    update();
  }

  void removeFromCart(String? id) {
    cartItemsList.removeWhere((element) => element.id == id);
    SnackMessage().showSnackBar("Item Removed");
    update();
  }

  void removeFromFavorite(String? id) {
    favoriteList.removeWhere((element) => element.id == id);
    update();
  }

  void increment() {
    cartCounter++;
    update();
  }

  void decrement() {
    if (cartCounter.value != 0) cartCounter--;
    update();
  }
}
