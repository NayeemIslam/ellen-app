import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  var bottomIndex = 0.obs;
  var tabBarIndex = 0.obs;
  var topSearch = <String>[].obs;
  var remeberMe = false.obs;
  var acceptCondtions = false.obs;
  var paymentMethodIndex = 0.obs;
  var sortBy = "Ratings".obs;
  var levelBy = 0.obs;
  var ratingsBy = 0.obs;
  var priceBy = 0.obs;

  var userExists = true.obs;
  ScrollController scrollController = ScrollController();
  ScrollPhysics physics = const ClampingScrollPhysics();

  @override
  onInit() {
    super.onInit();
    scrollController.addListener(scroll);
  }

  scroll() {
    if (scrollController.position.pixels <= 56) {
      physics = const ClampingScrollPhysics();
    } else {
      physics = const BouncingScrollPhysics();
    }
    update();
  }

  PageController pageViewCtrl =
      PageController(initialPage: 0, keepPage: true, viewportFraction: 1);

  changeHomePage(int index) {
    bottomIndex(index);
    pageViewCtrl.animateToPage(index,
        duration: 500.milliseconds, curve: Curves.ease);
    update();
  }

  changeTab(int index) {
    tabBarIndex(index);
    update();
  }

  addTopSearch(String v) {
    topSearch.add(v);
    update();
  }

  void changeRemember(bool v) {
    remeberMe(v);
    update();
  }

  void changeAcceptCondition(bool v) {
    acceptCondtions(v);
    update();
  }

  void changePaymetMethod(int v) {
    paymentMethodIndex(v);
    update();
  }

  void changeAuth(bool v) {
    userExists(v);
    update();
  }

  void changeSortBy(String value) {
    sortBy(value);
    update();
  }

  void changeLevelBy(int v) {
    levelBy(v);
    update();
  }

  void changePriceBy(int v) {
    priceBy(v);
    update();
  }

  void changeRatingsBy(int v) {
    ratingsBy(v);
    update();
  }
}
