import 'package:ellen_learning_app/module/model/course_model.dart';
import 'package:ellen_learning_app/utils/services/data_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class DataController extends GetxController {
  var isLoading = false.obs;
  var courseList = <CourseModel>[].obs;
  var courseDetails = CourseModel().obs;
  var instructorInfo = Instructor().obs;

  @override
  void onInit() {
    super.onInit();
    getAllCourse();
  }

  void getAllCourse() async {
    try {
      isLoading(true);
      final result = await ServiceApi.instance.courseNetworkCall();
      if (result != null) {
        courseList.assignAll(result);
      }
      update();
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      isLoading(false);
    }
  }

  void fetchCourseDetails(CourseModel value) {
    courseDetails.value = value;
    update();
  }

  void fetchInstructorInfo(Instructor value) {
    instructorInfo.value = value;
    update();
  }
}
