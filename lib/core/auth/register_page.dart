import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../config/route/app_route.dart';
import '../../constants/app_color.dart';
import '../../constants/app_constants.dart';
import '../../constants/responsive_size.dart';
import '../../module/controller/app_controller.dart';
import '../../widgets/custom_button.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Get.back(),
            icon: SvgPicture.asset(
              back,
              // height: getScreeWidth(24),
              // width: getScreeWidth(24),
            ),
          ),
          title: const Text("Register"),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          children: [
            Image.asset(registerImage),
            getVerticalSpace(20),
            TextField(
              decoration: textInputDecoration(hint: "Username"),
            ),
            getVerticalSpace(15),
            TextField(
              decoration: textInputDecoration(hint: "Email Address"),
            ),
            getVerticalSpace(10),
            TextField(
              decoration: textInputDecoration(hint: "Phone"),
            ),
            getVerticalSpace(10),
            TextField(
              obscureText: true,
              decoration: textInputDecoration(hint: "Password"),
            ),
            getVerticalSpace(10),
            TextField(
              obscureText: true,
              decoration: textInputDecoration(hint: "Confirm Password"),
            ),
            getVerticalSpace(15),
            Row(
              children: [
                Radio<bool>(
                    value: true,
                    groupValue: appCtrl.acceptCondtions.value,
                    onChanged: (v) {
                      appCtrl.changeAcceptCondition(v!);
                    }),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: getTextSize(14),
                      fontWeight: FontWeight.w600,
                      color: kLightGreyColor,
                      fontFamily: 'JosefinSans',
                    ),
                    text: "I accept all\t",
                    children: [
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => debugPrint("Go to register"),
                        style: TextStyle(
                          fontSize: getTextSize(14),
                          fontWeight: FontWeight.w600,
                          color: kDarkText,
                          fontFamily: 'JosefinSans',
                        ),
                        text: "Terms & Conditions",
                      ),
                    ],
                  ),
                ),
              ],
            ),
            CustomButton(
              title: "Register Now",
              color: kDarkText,
              press: () {},
            ),
            getVerticalSpace(30),
            RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: getTextSize(14),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                  fontFamily: 'JosefinSans',
                ),
                text: "Already have an account?\t",
                children: [
                  TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        try {
                          Get.toNamed(AppRoute.loginPage);
                        } catch (e) {
                          debugPrint(e.toString());
                        }
                      },
                    style: TextStyle(
                      fontSize: getTextSize(16),
                      fontWeight: FontWeight.w600,
                      color: kRedColor,
                      fontFamily: 'JosefinSans',
                    ),
                    text: "Login",
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
