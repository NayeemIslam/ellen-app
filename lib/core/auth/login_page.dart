import 'package:ellen_learning_app/constants/assets_path.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../config/route/app_route.dart';
import '../../constants/app_color.dart';
import '../../constants/app_constants.dart';
import '../../constants/responsive_size.dart';
import '../../module/controller/app_controller.dart';
import '../../widgets/custom_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    AppController appCtrl = Get.find();
    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Get.back(),
            icon: SvgPicture.asset(
              back,
              // height: getScreeWidth(24),
              // width: getScreeWidth(24),
            ),
          ),
          title: const Text("Login"),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          children: [
            Image.asset(loginImage),
            getVerticalSpace(20),
            TextField(
              decoration: textInputDecoration(hint: "Email Address"),
            ),
            getVerticalSpace(10),
            TextField(
              obscureText: true,
              decoration: textInputDecoration(hint: "Password"),
            ),
            getVerticalSpace(15),
            Row(
              children: [
                Radio<bool>(
                    value: true,
                    groupValue: appCtrl.remeberMe.value,
                    onChanged: (v) {
                      appCtrl.changeRemember(v!);
                    }),
                Text(
                  "Remember Me",
                  style: TextStyle(
                    fontSize: getTextSize(14),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xff565657),
                  ),
                )
              ],
            ),
            CustomButton(
              title: "Login",
              color: kDarkText,
              press: () {},
            ),
            getVerticalSpace(30),
            RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: getTextSize(14),
                  fontWeight: FontWeight.w600,
                  color: kDarkText,
                  fontFamily: 'JosefinSans',
                ),
                text: "Don't have an account?\t",
                children: [
                  TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        try {
                          Get.toNamed(AppRoute.registerPage);
                        } catch (e) {
                          debugPrint(e.toString());
                        }
                      },
                    style: TextStyle(
                      fontSize: getTextSize(16),
                      fontWeight: FontWeight.w600,
                      color: kRedColor,
                      fontFamily: 'JosefinSans',
                    ),
                    text: "Register",
                  ),
                ],
              ),
            ),
            getVerticalSpace(20),
          ],
        ),
      );
    });
  }
}
