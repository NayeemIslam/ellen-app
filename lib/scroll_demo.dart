import 'package:flutter/material.dart';

class ScrollDemo extends StatefulWidget {
  const ScrollDemo({Key? key}) : super(key: key);

  @override
  _ScrollDemoState createState() => _ScrollDemoState();
}

class _ScrollDemoState extends State<ScrollDemo> {
  ScrollController scrollController = ScrollController();
  ScrollPhysics _physics = const ClampingScrollPhysics();

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels <= 56) {
        _physics = const ClampingScrollPhysics();
      } else {
        _physics = const BouncingScrollPhysics();
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose(); //always dispose the controller
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Scroll Physics Demo"),
      ),
      body: ListView.builder(
        controller: scrollController,
        physics: _physics,
        itemCount: 20,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        itemBuilder: (context, index) {
          return Card(
            elevation: 1,
            child: ListTile(
              leading: CircleAvatar(
                child: Text("$index"),
              ),
              title: Text("#$index Item"),
            ),
          );
        },
      ),
    );
  }
}
